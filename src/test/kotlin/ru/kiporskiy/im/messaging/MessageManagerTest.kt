package ru.kiporskiy.im.messaging

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.help.MAX_SENT_MESSAGES_PORTION_SIZE
import ru.kiporskiy.im.help.REPEAT_MESSAGES_SEND_DURATION
import java.util.concurrent.ThreadLocalRandom

internal class MessageManagerTest {

    private lateinit var manager: MessageManager

    fun getMessageMock(id: Long = ThreadLocalRandom.current().nextLong(), ack: Boolean = true): Message {
        val result = mock<Message>()
        whenever(result.id).thenReturn(id)
        whenever(result.ack).thenReturn(ack)
        return result
    }

    @BeforeEach
    internal fun setUp() {
        manager = MessageManager()
    }

    @Test
    internal fun addMessage() {
        val id = 0L
        val message = getMessageMock(id)

        manager.addMessage(message)

        val messages = manager.getMessages()

        assertEquals(1, messages.size)
        assertEquals(message, messages[0])

        assertTrue(manager.getMessages().isEmpty())
    }

    @Test
    internal fun addSeveralMessages() {
        val message1 = getMessageMock()
        val message2 = getMessageMock()
        val message3 = getMessageMock()

        manager.addMessage(message1)
        manager.addMessage(message2)
        manager.addMessage(message3)

        val messages = manager.getMessages()

        assertEquals(3, messages.size)
        assertEquals(message1, messages[0])
        assertEquals(message2, messages[1])
        assertEquals(message3, messages[2])

        assertTrue(manager.getMessages().isEmpty())
    }

    @Test
    internal fun addSeveralManyMessages() {
        for (i in 0 until MAX_SENT_MESSAGES_PORTION_SIZE * 2 + 1) {
            val message = getMessageMock()
            manager.addMessage(message)
        }

        var messages = manager.getMessages()
        assertEquals(MAX_SENT_MESSAGES_PORTION_SIZE, messages.size)
        messages = manager.getMessages()
        assertEquals(MAX_SENT_MESSAGES_PORTION_SIZE, messages.size)
        messages = manager.getMessages()
        assertEquals(1, messages.size)

        assertTrue(manager.getMessages().isEmpty())
    }

    @Test
    internal fun tooManyMessages() {
        val ids = HashSet<Long>()
        for (i in 0L until MessageManager.MAX_MESSAGES_QUEUE) {
            val message = getMessageMock()
            manager.addMessage(message)
            if (i >= MessageManager.MAX_MESSAGES_QUEUE * 0.1) {
                ids.add(message.id)
            }
        }

        var k: Int
        val idsResult = HashSet<Long>()
        do {
            val messages = manager.getMessages()
            k = messages.size
            idsResult.addAll(messages.map { it.id })
        } while (k > 0)

        assertEquals(idsResult, ids)
    }

    @Test
    internal fun sentMessage_notSuccess() {
        val message = getMessageMock()

        manager.addMessage(message)
        manager.getMessages()
        assertEquals(0, manager.getMessages().size)
        manager.sentMessages(listOf(message), false)
        assertEquals(1, manager.getMessages().size)
    }

    @Test
    internal fun sentMessage_success() {
        val message = getMessageMock()

        manager.addMessage(message)
        manager.getMessages()
        assertEquals(0, manager.getMessages().size)
        manager.sentMessages(listOf(message), true)
        assertEquals(0, manager.getMessages().size)
        manager.sentMessages(listOf(message), false)
        assertEquals(0, manager.getMessages().size)
    }

    @Test
    @Disabled("Too long execute")
    internal fun resend() {
        val message = getMessageMock()

        manager.addMessage(message)
        manager.getMessages()
        assertEquals(0, manager.getMessages().size)
        manager.sentMessages(listOf(message))
        Thread.sleep(REPEAT_MESSAGES_SEND_DURATION.toMillis() + 100)
        assertEquals(1, manager.getMessages().size)
    }

    @Test
    internal fun ackMessages() {
        val message = getMessageMock()

        manager.addMessage(message)
        manager.ack(setOf(message.id))
        assertEquals(0, manager.getMessages().size)
    }
}
