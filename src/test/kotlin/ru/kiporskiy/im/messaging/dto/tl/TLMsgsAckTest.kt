package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class TLMsgsAckTest : CodecTest() {

    override fun getInstance(): List<TLObject> {
        return listOf(MsgsAck(LongArray(10) { it.toLong() }))
    }

}
