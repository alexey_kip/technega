package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest

internal class TLInputUserTest : CodecTest() {

    override fun getInstance() = listOf(
            InputUserEmpty,
            InputUserFromMessage(InputPeerEmpty, 1, 2),
            InputUser(111, 222L),
            InputUserSelf
    )

}
