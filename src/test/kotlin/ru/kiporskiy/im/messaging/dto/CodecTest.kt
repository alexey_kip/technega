package ru.kiporskiy.im.messaging.dto

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

abstract class CodecTest {

    private val utils = TLObjectUtilsImpl()

    abstract fun getInstance(): List<TLObject>

    @Test
    internal fun runTest() {
        getInstance().forEach {
            val bytes = utils.serialize(it)
            val result = utils.deserializeData(bytes)
            Assertions.assertEquals(it, result)
        }
    }
}
