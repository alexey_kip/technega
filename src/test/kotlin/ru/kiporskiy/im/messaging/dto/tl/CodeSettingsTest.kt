package ru.kiporskiy.im.messaging.dto.tl

import org.junit.jupiter.api.Assertions.*
import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class CodeSettingsTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(CodeSettings(true, false, true),
            CodeSettings(false, true, false))
    }

}

