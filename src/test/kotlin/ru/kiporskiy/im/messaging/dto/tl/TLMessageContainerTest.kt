package ru.kiporskiy.im.messaging.dto.tl

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.TLObjectUtilsImpl

internal class TLMessageContainerTest : CodecTest() {

    private val utils = TLObjectUtilsImpl()

    override fun getInstance(): List<TLObject> {
        val it1 = Message(123L, 111, BooleanFalse)
        val it2 = Message(1234L, 110, BooleanTrue)
        val it3 = Message(12345L, 112, BooleanFalse)
        return listOf(MessageContainer(arrayOf(it1, it2, it3)))
    }

    @Test
    internal fun deserializeMessage() {
        val it = Message(123L, 111, BooleanFalse)
        val bytes = utils.serialize(it)
        val result = utils.deserializeMessage(bytes)
        Assertions.assertEquals(it, result)
    }
}
