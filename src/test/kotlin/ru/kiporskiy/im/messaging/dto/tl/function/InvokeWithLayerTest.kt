package ru.kiporskiy.im.messaging.dto.tl.function

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class InvokeWithLayerTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(InvokeWithLayer(222, GetConfig))
    }

}