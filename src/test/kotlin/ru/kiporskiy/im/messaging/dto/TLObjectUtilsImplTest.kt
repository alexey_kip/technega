package ru.kiporskiy.im.messaging.dto

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.messaging.dto.tl.InputPeerChannel
import ru.kiporskiy.im.messaging.dto.tl.InputPeerEmpty
import ru.kiporskiy.im.messaging.dto.tl.InputPeerUserFromMessage
import ru.kiporskiy.im.messaging.dto.tl.TLBoolean

internal class TLObjectUtilsImplTest {

    lateinit var serializer: TLObjectUtilsImpl

    @BeforeEach
    internal fun setUp() {
        this.serializer = TLObjectUtilsImpl()
    }

    @Test
    internal fun serializeBooleanFalse() {
        val obj = TLBoolean.of(false)
        val result = this.serializer.deserializeData(this.serializer.serialize(obj))
        assertEquals(obj, result)
    }

    @Test
    internal fun serializeBooleanTrue() {
        val obj = TLBoolean.of(false)
        val result = this.serializer.deserializeData(this.serializer.serialize(obj))
        assertEquals(obj, result)
    }

    @Test
    internal fun serializeInputPeerChannel() {
        val obj = InputPeerChannel(1235, 22L)

        val result = this.serializer.deserializeData(this.serializer.serialize(obj))
        assertEquals(obj, result)
    }

    @Test
    internal fun serializeInputPeerUserFromMessage() {
        val obj = InputPeerUserFromMessage(InputPeerEmpty, 1123, 334)

        val result = this.serializer.deserializeData(this.serializer.serialize(obj))
        assertEquals(obj, result)
    }
}
