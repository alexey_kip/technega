package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest

internal class RpcErrorTest : CodecTest() {
    override fun getInstance() = listOf(RpcError(123, "Test"))

}
