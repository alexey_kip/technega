package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.help.TimeUtils
import ru.kiporskiy.im.messaging.dto.CodecTest
import java.time.Instant

import java.time.LocalDateTime

internal class TLConfigTest : CodecTest() {
    override fun getInstance() = listOf(
            Config(true, false, true, false,
                    true, false, true,
                    Instant.ofEpochSecond(TimeUtils.localDateTimeToEpochSecond(LocalDateTime.of(2019, 1, 1, 12, 32)).toLong()),
                    LocalDateTime.of(2019, 1, 2, 12, 33),
                    TLBoolean.of(true), 1, arrayOf(
                    DcOption(true, false, false, true, true, 1, "2", 3, null)
            ), "Test",
                    1, 2, 3, 4, 5,
                    6, 7, 8, 9,
                    10, 11, 22, 34, 56,
                    1, 23, 4, 5, 1324,
                    123, 2455, 546, 456,
                    4555, 556, 6455, "prefix",
                    "", "username", "", "img",
                    "static", 234, 555, 665, "",
                    "", "")
    )
}
