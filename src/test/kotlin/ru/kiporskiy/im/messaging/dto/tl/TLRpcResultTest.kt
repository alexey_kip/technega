package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class TLRpcResultTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(RpcResult(213123L, BooleanFalse))
    }

}
