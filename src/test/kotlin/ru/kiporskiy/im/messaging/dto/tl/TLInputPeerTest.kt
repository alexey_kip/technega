package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest

internal class TLInputPeerTest : CodecTest() {

    override fun getInstance() = listOf(
            InputPeerChannel(1233, 333L),
            InputPeerChannelFromMessage(InputPeerSelf, 23333, 3345),
            InputPeerSelf,
            InputPeerEmpty,
            InputPeerChat(12223),
            InputPeerUserFromMessage(InputPeerEmpty, 223, 333),
            InputPeerUser(22333, 3334L)
    )

}
