package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest

internal class TLBooleanTest : CodecTest() {

    override fun getInstance() = listOf(BooleanTrue, BooleanFalse)

}
