package ru.kiporskiy.im.messaging.dto.tl.function

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class InitConnectionTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(InitConnection(123, "qwe", "rt", "1223",
                "13e", "langpack", "langCode", GetConfig))
    }

}