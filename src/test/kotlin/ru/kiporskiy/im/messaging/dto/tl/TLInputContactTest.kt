package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest

internal class TLInputContactTest : CodecTest() {
    override fun getInstance() = listOf(InputPhoneContact(123L, "phone", "Test", "Last"))
}
