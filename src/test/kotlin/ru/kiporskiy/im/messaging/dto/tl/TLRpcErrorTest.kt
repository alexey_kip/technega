package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class TLRpcErrorTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(RpcError(213123, "Test"))
    }

}
