package ru.kiporskiy.im.messaging.dto.tl.function

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.tl.CodeSettings

internal class SendCodeTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(
            SendCode(
                "123",
                123,
                "123",
                CodeSettings(false, true, true)
            )
        )
    }

}
