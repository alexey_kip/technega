package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class DcOptionTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(DcOption(true, false, false, true, true, 1, "2", 3, null),
                DcOption(false, true, false, false, true, 1, "2", 3, ByteArray(0)))
    }

}
