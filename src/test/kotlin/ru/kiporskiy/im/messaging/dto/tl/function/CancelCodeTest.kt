package ru.kiporskiy.im.messaging.dto.tl.function

import org.junit.jupiter.api.Assertions.*
import ru.kiporskiy.im.messaging.dto.CodecTest
import ru.kiporskiy.im.messaging.dto.TLObject

internal class CancelCodeTest : CodecTest() {
    override fun getInstance(): List<TLObject> {
        return listOf(CancelCode("90999000", "test"))
    }
}
