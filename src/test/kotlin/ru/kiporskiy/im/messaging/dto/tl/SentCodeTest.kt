package ru.kiporskiy.im.messaging.dto.tl

import org.junit.jupiter.api.Assertions.*
import ru.kiporskiy.im.messaging.dto.CodecTest

internal class SentCodeTest : CodecTest() {
    override fun getInstance() = listOf(SentCode(SentCodeTypeSms(123), "Str",
        CodeTypeSms, 123))
}
