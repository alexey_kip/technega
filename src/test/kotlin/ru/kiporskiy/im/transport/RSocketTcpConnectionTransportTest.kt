package ru.kiporskiy.im.transport

import io.rsocket.SocketAcceptor.forRequestChannel
import io.rsocket.util.DefaultPayload
import org.junit.jupiter.api.*
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import ru.kiporskiy.im.IntegrationTest
import ru.kiporskiy.im.RSocketTestServer

@IntegrationTest
internal class RSocketTcpConnectionTransportTest {

    @BeforeEach
    internal fun setUp() {
        ServersManager.reset()
    }

    @AfterEach
    internal fun tearDown() {
        ServersManager.reset()
    }

    companion object {
        private val log = LoggerFactory.getLogger(RSocketTcpConnectionTransportTest::class.java)
    }

    @Test
    @DisplayName("Проверка транспорта на предмет успешной отправки/получения данных")
    internal fun connection() {
        //запуск сервера на порту 11234
        val port = 11234

        val server = RSocketTestServer(port, forRequestChannel { _ ->
            //при получении любых данных отвечать pong
            Flux.just(DefaultPayload.create("pong"))
        })

        //старт сервера
        server.runServer()

        val address = DcAddress(1, "localhost", port)
        val transport = RSocketTcpConnectionTransport()

        //подключиться к серверу
        transport.connect(address)

        //при получении результата прочитать его в utf-8
        var result = ""
        transport.onInputPayload { result = it.dataUtf8 }

        //отправить строку ping
        val payload = "ping"
        transport.write(DefaultPayload.create(payload), { log.info("Success: $it") })

        //небольшое ожидание ответа
        Thread.sleep(500)

        server.close()

        //результатом должен быть pong
        val resultPayload = "pong"
        Assertions.assertEquals(resultPayload, result)
    }
}
