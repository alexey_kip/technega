package ru.kiporskiy.im.transport.binary

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import ru.kiporskiy.im.transport.binary.BufferStateException
import ru.kiporskiy.im.transport.binary.ByteBufWrapper

internal class ByteBufWrapperTest {

    private lateinit var buf: ByteBufWrapper

    @BeforeEach
    internal fun setUp() {
        this.buf = ByteBufWrapper(16, false)
    }

    @Test
    internal fun readWriteInt() {
        val k = 1023
        val result = this.buf.write(k).readInt()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteLong() {
        val k = 1023 * 13L
        val result = this.buf.write(k).readLong()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteBoolean() {
        val b = false
        val result = this.buf.write(b).readBoolean()
        assertEquals(b, result)
    }

    @Test
    internal fun readWriteDouble() {
        val k = 1023 * 13.1
        val result = this.buf.write(k).readDouble()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteString() {
        val k = "Test"
        val result = this.buf.write(k).readString()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteString_252() {
        val k = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "1234567890123456789012345678901234567890123456789012"
        val result = this.buf.write(k).readString()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteString_256() {
        val k = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "12345678901234567890123456789012345678901234567890123456"
        val result = this.buf.write(k).readString()
        assertEquals(k, result)
    }

    @Test
    internal fun readWriteString_260() {
        val k = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                "123456789012345678901234567890123456789012345678901234567123"
        val result = this.buf.write(k).readString()
        assertEquals(k, result)
    }

    @Test
    internal fun closeBuffer() {
        this.buf.close()
        try {
            this.buf.readBoolean()
            fail("")
        } catch (error: BufferStateException) {
        }
    }
}
