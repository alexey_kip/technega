package ru.kiporskiy.im.transport

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.IntegrationTest

@IntegrationTest
internal class InternetConnectionTesterTest {

    @Test
    @DisplayName("Проверка наличия подключения к интернету")
    internal fun testInternetConnection() {
        assertTrue(InternetConnectionTester.check())
    }
}
