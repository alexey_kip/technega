package ru.kiporskiy.im.transport

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.dto.TLObjectUtils
import ru.kiporskiy.im.messaging.router.AcknowledgeMessageRouter
import ru.kiporskiy.im.transport.binary.ByteWrapper

internal class RecipientImplTest {

    lateinit var recipient: RecipientImpl

    lateinit var utils: TLObjectUtils

    lateinit var router: AcknowledgeMessageRouter

    @BeforeEach
    internal fun setUp() {
        this.utils = mock()
        this.router = mock()
        this.recipient = RecipientImpl(this.utils)
        this.recipient.addMsgHandler(router)
    }

    @Test
    @DisplayName("Тестирует правильность обработки потока входящих данных")
    internal fun correctInputData() {
        val data = mock<ByteWrapper>()
        val message: Message = mock()
        val response = listOf(message)
        whenever(utils.deserialize(data)).thenReturn(response)
        this.recipient.inputData(data)
        verify(this.router).routing(message)
    }
}
