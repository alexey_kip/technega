package ru.kiporskiy.im.transport

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows

internal class ServersManagerTest {

    @BeforeEach
    internal fun setUp() {
        ServersManager.reset()
    }

    @AfterEach
    internal fun tearDown() {
        ServersManager.reset()
    }

    @Test
    internal fun get_error_serversListIsEmpty() {
        assertThrows(IllegalStateException::class.java) { ServersManager.get() }
    }

    @Test
    internal fun get_ok_oneServer() {
        val dc = mock<DcAddress>()
        ServersManager.setServerAddresses(setOf(dc))

        for (i in 0 until 1000) {
            assertEquals(dc, ServersManager.get())
        }
    }

    @Test
    internal fun get_ok_severalServer() {
        val dc1 = mock<DcAddress>()
        val dc2 = mock<DcAddress>()

        ServersManager.setServerAddresses(setOf(dc1, dc2))

        if (dc1 == ServersManager.get(false)) {
            for (i in 0 until ServersManager.MAX_CONNECTIONS_COUNTER) {
                assertEquals(dc1, ServersManager.get())
            }

            for (i in 0 until ServersManager.MAX_CONNECTIONS_COUNTER) {
                assertEquals(dc2, ServersManager.get())
            }

            assertEquals(dc1, ServersManager.get())
        } else {
            for (i in 0 until ServersManager.MAX_CONNECTIONS_COUNTER) {
                assertEquals(dc2, ServersManager.get())
            }

            for (i in 0 until ServersManager.MAX_CONNECTIONS_COUNTER) {
                assertEquals(dc1, ServersManager.get())
            }

            assertEquals(dc2, ServersManager.get())
        }
    }
}
