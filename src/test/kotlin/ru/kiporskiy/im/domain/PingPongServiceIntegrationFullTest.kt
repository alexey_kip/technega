package ru.kiporskiy.im.domain

import io.netty.buffer.Unpooled
import io.rsocket.SocketAcceptor
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import reactor.core.publisher.Flux
import ru.kiporskiy.im.IntegrationTest
import ru.kiporskiy.im.RSocketTestServer
import ru.kiporskiy.im.TechnoClientBootstrap
import ru.kiporskiy.im.domain.service.MasterService
import ru.kiporskiy.im.event.EventManager
import ru.kiporskiy.im.help.RandomUtils
import ru.kiporskiy.im.messaging.*
import ru.kiporskiy.im.messaging.dto.TLObjectUtils
import ru.kiporskiy.im.messaging.dto.TLObjectUtilsImpl
import ru.kiporskiy.im.messaging.dto.tl.Ping
import ru.kiporskiy.im.messaging.dto.tl.Pong
import ru.kiporskiy.im.transport.*
import ru.kiporskiy.im.transport.binary.ByteBufWrapper
import ru.kiporskiy.im.transport.event.ConnectionEvent
import kotlin.test.assertTrue

@IntegrationTest
internal class PingPongServiceIntegrationFullTest {

    private val serverPort = 11234

    private lateinit var server: RSocketTestServer

    private lateinit var codec: TLObjectUtils

    @BeforeEach
    internal fun setUp() {
        this.codec = TLObjectUtilsImpl()

        server = RSocketTestServer(serverPort, SocketAcceptor.forRequestChannel { payloads ->
            Flux.from(payloads)
                //десериализация полученных байтов
                .map { p -> this.codec.deserialize(ByteBufWrapper(Unpooled.copiedBuffer(p.data))) }
                // проверка, что реально получен один Ping
                .filter { req -> req.size == 1 && req[0].data is Ping }
                //формирование Pong
                .map { Pong(it[0].id, (it[0].data as Ping).pingId) }
                //создание сообщения
                .map { Message.of(it, RandomUtils.nextLong(), false) }
                //сериализация сообщения
                .map { this.codec.serialize(it) }
                //получение Payload
                .map { it.getPayload() }
        })

        server.runServer()
    }

    @AfterEach
    internal fun tearDown() {
        server.close()
    }

    @Test
    @DisplayName("Отправить на сервер пинг и получить от него pong")
    internal fun sendPingRequestPong() {
        //адрес сервера
        val address = DcAddress(1, "localhost", serverPort)

        TechnoClientBootstrap(arrayOf(address)).run()

        EventManager.subscribe(ConnectionEvent.EVENT_TYPE) { event ->
            require(event is ConnectionEvent)
            if (event.connected) MasterService.pingPongService.sendPing()
        }

        Thread.sleep(1000)

        assertTrue { MasterService.pingPongService.getLastDelay() > 0L }
    }
}
