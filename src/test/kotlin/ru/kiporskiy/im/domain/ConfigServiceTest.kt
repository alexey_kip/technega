package ru.kiporskiy.im.domain

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.core.help.Config
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.service.ConfigService
import ru.kiporskiy.im.help.TimeSettings
import java.lang.Thread.sleep
import java.time.Instant

internal class ConfigServiceTest {

    lateinit var mediator: Mediator

    lateinit var helpMediator: HelpMediator

    @BeforeEach
    internal fun setUp() {
        mediator = mock()
        helpMediator = mock()
        doReturn(helpMediator).whenever(mediator).help
    }

    @Test
    @DisplayName("Тест проверяет, что после создания инстанса класса ConfigRequesterHandler сразу происходит " +
            "отправка запроса конфигов на сервер")
    internal fun getConfig_runOnStartup() {
        ConfigService(mediator)

        //дать время на автоматическую отправку (она рпоисходит в фоновом потоке)
        sleep(500)

        //проверить, что был выполнен запрос конфигов от сервера
        verify(helpMediator).getConfig(eq(true), any(), any())
    }

    @Test
    @DisplayName("Тест проверяет, что после создания инстанса класса ConfigRequesterHandler и получения первых " +
            "конфигов от сервера второй запрос не будет отправлять информацию о клиенте")
    internal fun getConfig_doNotSendInfo() {
        //после первого запроса нужно смоделировать ситуацию получения ответа
        doAnswer { r ->
            val config: Config = mock()
            whenever(config.date).thenReturn(Instant.now())
            val arg = r.getArgument(1) as ((configResult: Config) -> Unit)
            arg.invoke(config)
        }.whenever(helpMediator).getConfig(any(), any(), any())

        val handel = ConfigService(mediator)

        //первый запрос улетает автоматически
        sleep(500)

        //сбросить статистику запросов
        reset(helpMediator)

        //отправить второй запрос
        handel.updateConfigs()

        //проверить, что второй запрос не требует повторной отправки информации о клиенте
        verify(helpMediator).getConfig(eq(false), any(), any())
    }

    @Test
    @DisplayName("Тест проверяет, что после получения ответа от сервера происходит вызов функции, отвечающей за " +
            "обработку ответа")
    internal fun getConfig_runOnResponse() {
        //время, которое должно будет прилететь от сервера в конфигах
        val serverTime = Instant.ofEpochSecond(Instant.now().epochSecond - 1)

        //после запроса нужно смоделировать ситуацию получения ответа
        doAnswer { r ->
            val config: Config = mock()
            val arg = r.getArgument(1) as ((configResult: Config) -> Unit)
            whenever(config.date).thenReturn(serverTime)
            arg.invoke(config)
        }.whenever(helpMediator).getConfig(any(), any(), any())

        ConfigService(mediator)

        //дать время на автоматическую отправку (она рпоисходит в фоновом потоке)
        sleep(500)

        Assertions.assertTrue(TimeSettings.getServerTimeDiffs() > 0 && TimeSettings.getServerTimeDiffs() <= 50L)
    }

    @Test
    @DisplayName("Тест проверяет, что после получения ошибки при выполнении запроса происходит вызов функции," +
            " отвечающей за обработку этой ошибки")
    internal fun getConfig_runOnError() {
        //после запроса нужно смоделировать ситуацию получения Ошибки
        doAnswer { r ->
            val config: Error = mock()
            val arg = r.getArgument(2) as ((error: Error) -> Unit)
            arg.invoke(config)
        }.whenever(helpMediator).getConfig(any(), any(), any())

        ConfigService(mediator)

        //дать время на автоматическую отправку (она происходит в фоновом потоке)
        sleep(300)

        //проверить, что был выполнен запрос конфигов от сервера
        verify(helpMediator).getConfig(eq(true), any(), any())

        //дать время для повторного запуска запроса конфигов
        sleep(1200)

        //проверить, что был выполнен запрос конфигов от сервера
        verify(helpMediator, times(2)).getConfig(eq(true), any(), any())
    }
}
