package ru.kiporskiy.im.domain

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.domain.service.PingPongService

internal class PingPongServiceTest {

    lateinit var service: PingPongService

    lateinit var mediator: Mediator

    lateinit var helpMediator: HelpMediator

    lateinit var serverConnectionStatus: ServerConnectionStatus

    @BeforeEach
    internal fun setUp() {
        this.mediator = mock()
        this.helpMediator = mock()
        this.serverConnectionStatus = mock()
        this.service = PingPongService(
            this.mediator,
            this.serverConnectionStatus
        )
        doReturn(true).whenever(serverConnectionStatus).isConnected()
        doReturn(this.helpMediator).whenever(this.mediator).help
    }

    @Test
    @DisplayName("Автоматическая отправка пинга через 10 секунд после создания экземпляра")
    @Disabled("Долгий тест")
    internal fun sendPing_autoAfter10Sec() {
        verify(this.helpMediator, never()).runPing(any())
        verify(this.helpMediator, timeout(PingPongService.PING_DELAY + 100)).runPing(any())
    }

    @Test
    @DisplayName("Ручная отправка пинга")
    internal fun sendPing_OK() {
        this.service.sendPing()
        verify(this.helpMediator).runPing(any())
    }

    @Test
    @DisplayName("Отправка pong - ответа на ping")
    internal fun sendPong_OK() {
        val msgId = 100L
        val pingId = 200L
        this.service.sendPong(msgId, pingId)
        verify(this.helpMediator).runPong(msgId, pingId)
    }

    @Test
    @DisplayName("Получен ответ Pong")
    internal fun onPong_OK() {
        val pingId = 200L
        this.service.onPong(pingId)
    }
}
