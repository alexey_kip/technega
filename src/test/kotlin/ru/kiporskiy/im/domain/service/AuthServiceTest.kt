package ru.kiporskiy.im.domain.service

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.*
import ru.kiporskiy.im.core.auth.SentCode
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.AuthMediator
import ru.kiporskiy.im.domain.Mediator
import ru.kiporskiy.im.transport.ServersManager
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class AuthServiceTest {

    private lateinit var mediator: Mediator

    private lateinit var auth: AuthMediator

    private lateinit var service: AuthService

    @BeforeEach
    internal fun setUp() {
        mediator = mock()
        auth = mock()
        service = AuthService(mediator)

        doReturn(auth).whenever(mediator).auth
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    @DisplayName(
        "Тест проверяет, что был вызван нужный метод для отправки кода и отработан результат успешного " +
                "выполнения запроса"
    )
    internal fun sentCodeOk() {
        var resultPhone = "NO"
        val testPhone = "911"
        val sentCodeResult: SentCode = mock()

        //при попытке вызвать auth.sendCode выполнить лямбду во втором аргументе
        doAnswer {
            (it.arguments[1] as  (sentCodeResult: SentCode) -> Unit).invoke(sentCodeResult)
        }.whenever(auth).sendCode(any(), any(), any())

        //тестовый запуск
        service.sendCode(testPhone, { resultPhone = testPhone }, { fail(IllegalStateException()) })

        //проверка успешного выполнения
        assertEquals(resultPhone, testPhone)
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    @DisplayName(
        "Тест проверяет, что был вызван нужный метод для отправки кода и отработан результат неуспешного " +
                "выполнения запроса"
    )
    internal fun sentCodeFalse() {
        var resultPhone = "NO"
        val testPhone = "911"
        val sentCodeError: Error = mock()

        //при попытке вызвать auth.sendCode выполнить лямбду в третьем аргументе
        doAnswer {
            (it.arguments[2] as  (error: Error) -> Unit).invoke(sentCodeError)
        }.whenever(auth).sendCode(any(), any(), any())

        //тестовый запуск
        service.sendCode(testPhone, { fail(IllegalStateException()) }, { resultPhone = testPhone } )

        //проверка успешного выполнения
        assertEquals(resultPhone, testPhone)
    }

    @Test
    @DisplayName("Тест проверяет, что метод не вызывается несколько раз подряд")
    internal fun sendCodeSeveral() {
        val testPhone = "911"

        service.sendCode(testPhone, {}, {} )

        service.sendCode(testPhone, {}, {} )

        verify(auth, times(1)).sendCode(any(), any(), any())
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    @DisplayName(
        "Тест проверяет, что повторный код был успешно отправлен"
    )
    internal fun resentCodeOk() {
        var resultPhone = "NO"
        val testPhone = "911"
        val sentCodeResult: SentCode = mock()

        //при попытке вызвать auth.sendCode выполнить лямбду во втором аргументе
        doAnswer {
            (it.arguments[1] as  (sentCodeResult: SentCode) -> Unit).invoke(sentCodeResult)
        }.whenever(auth).sendCode(any(), any(), any())

        doReturn("Test").whenever(sentCodeResult).codeHash

        //при попытке вызвать auth.sendCode меняет значение проверочной переменной
        doAnswer {
            resultPhone = testPhone
            (it.arguments[2] as  (sentCodeResult: SentCode) -> Unit).invoke(sentCodeResult)
        }.whenever(auth).resendCode(any(), any(), any(), any())

        //первый запуск
        service.sendCode(testPhone, {  }, {  })

        //повторный запуск
        service.sendCode(testPhone, {  }, { fail(IllegalStateException()) })

        //проверка успешного выполнения
        assertEquals(resultPhone, testPhone)
    }

    @Test
    @DisplayName("Тест проверяет, что отмена кода не работает, если код не был отправлен")
    internal fun cancelCodeException() {
        Assertions.assertThrows(IllegalArgumentException::class.java) { service.cancelCode({}, {}) }
    }



    @Suppress("UNCHECKED_CAST")
    @Test
    @DisplayName(
        "Тест проверяет успешность отправки сброса отправленного кода"
    )
    internal fun cancelCodeOk() {
        val testPhone = "911"
        val sentCodeResult: SentCode = mock()
        var success = false

        doReturn("Test").whenever(sentCodeResult).codeHash

        //при попытке вызвать auth.sendCode выполнить лямбду во втором аргументе
        doAnswer {
            (it.arguments[1] as  (sentCodeResult: SentCode) -> Unit).invoke(sentCodeResult)
        }.whenever(auth).sendCode(any(), any(), any())

        //тестовый запуск
        service.sendCode(testPhone, {  }, {  })

        //при попытке вызвать auth.sendCode выполнить лямбду в третьем аргументе
        doAnswer {
            (it.arguments[2] as  (sentCodeResult: Boolean) -> Unit).invoke(true)
        }.whenever(auth).cancelCode(any(), any(), any(), any())

        service.cancelCode({ success = true}, {})

        //проверка успешного выполнения
        assertTrue(success)
    }
}
