package ru.kiporskiy.im.core.settings

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import ru.kiporskiy.im.core.channel.ChannelId
import ru.kiporskiy.im.core.channel.DefaultChannel
import ru.kiporskiy.im.core.chat.Chat
import ru.kiporskiy.im.core.chat.ChatId
import ru.kiporskiy.im.core.chat.DefaultChat
import ru.kiporskiy.im.core.peer.settings.NotifySettings
import ru.kiporskiy.im.core.user.BotUser
import ru.kiporskiy.im.core.user.UserId
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertSame

internal class PeerNotifySettingsTest {

    @BeforeEach
    internal fun setUp() {
        PeerNotifySettings.reset()
    }

    @Test
    @DisplayName("Тест проверяет, что после инициализации все настройки сброшены на дефолтные значения")
    internal fun getPeerDefaultSettings() {
        val defaultSettings = PeerNotifySettings.getDefaultSettings()

        val bot = BotUser(UserId(1, 2), "First", "username", NotifySettings.DEFAULT)
        val botSettings = PeerNotifySettings.getSettings(bot)
        assertEquals(botSettings, defaultSettings)

        val chat = DefaultChat(ChatId(2), "name", UserId(1, 2), participants = mapOf())
        val chatSettings = PeerNotifySettings.getSettings(chat)
        assertEquals(chatSettings, defaultSettings)

        val channel = DefaultChannel(
            ChannelId(2, 3L), UserId(1, 2), "name", "descr",
            mapOf(), null, null, NotifySettings.DEFAULT
        )
        val channelSettings = PeerNotifySettings.getSettings(channel)
        assertEquals(channelSettings, defaultSettings)
    }

    @Test
    @DisplayName("Тест проверяет, что после сохранения настроек пользователей они успешно возвращаются в дальнейшем при запросе")
    internal fun setAndGetSettingsUserSettings() {
        //если указать настройки для всех пользователей, то они будут возвращаться и для бота
        val bot = BotUser(UserId(1, 2), "First", "username", NotifySettings.DEFAULT)
        val userSettings = NotifySettings(true, Instant.now())
        PeerNotifySettings.updateUsersSettings(userSettings)
        assertEquals(userSettings, PeerNotifySettings.getSettings(bot))

        //если указать персональные настройки для бота, то будут возвращаться именно они
        val personalBotSettings = NotifySettings(false, Instant.now().plusSeconds(1000))
        PeerNotifySettings.updateSettings(bot, personalBotSettings)
        assertEquals(personalBotSettings, PeerNotifySettings.getSettings(bot))

        //если указать персональные настройки для бота с истекшим сроком, то будут возвращаться настройки всех пользователей
        val expiredPersonalBotSettings = NotifySettings(false, Instant.now().minusSeconds(1))
        PeerNotifySettings.updateSettings(bot, expiredPersonalBotSettings)
        assertEquals(userSettings, PeerNotifySettings.getSettings(bot))
    }

    @Test
    @DisplayName("Тест проверяет, что после сохранения настроек чатов они успешно возвращаются в дальнейшем при запросе")
    internal fun setAndGetSettingsChatSettings() {
        //если указать настройки для всех чатов, то они будут возвращаться и для указанного чата
        val chat = DefaultChat(ChatId(2), "name", UserId(1, 2), participants = mapOf())
        val chatSettings = NotifySettings(true, Instant.now())
        PeerNotifySettings.updateChatsSettings(chatSettings)
        assertEquals(chatSettings, PeerNotifySettings.getSettings(chat))

        //если указать персональные настройки для чата, то будут возвращаться именно они
        val personalChatSettings = NotifySettings(false, Instant.now().plusSeconds(1000))
        PeerNotifySettings.updateSettings(chat, personalChatSettings)
        assertEquals(personalChatSettings, PeerNotifySettings.getSettings(chat))

        //если указать персональные настройки для чата с истекшим сроком, то будут возвращаться настройки всех чатов
        val expiredPersonalChatSettings = NotifySettings(false, Instant.now().minusSeconds(1))
        PeerNotifySettings.updateSettings(chat, expiredPersonalChatSettings)
        assertEquals(chatSettings, PeerNotifySettings.getSettings(chat))
    }

    @Test
    @DisplayName("Тест проверяет, что после сохранения настроек каналов они успешно возвращаются в дальнейшем при запросе")
    internal fun setAndGetSettingsChannelsSettings() {
        //если указать настройки для всех каналов, то они будут возвращаться и для указанного канала
        val channel = DefaultChannel(
            ChannelId(2, 3L), UserId(1, 2), "name", "descr",
            mapOf(), null, null, NotifySettings.DEFAULT
        )
        val channelSettings = NotifySettings(true, Instant.now())
        PeerNotifySettings.updateChannelsSettings(channelSettings)
        assertEquals(channelSettings, PeerNotifySettings.getSettings(channel))

        //если указать персональные настройки для канала, то будут возвращаться именно они
        val personalChannelSettings = NotifySettings(false, Instant.now().plusSeconds(1000))
        PeerNotifySettings.updateSettings(channel, personalChannelSettings)
        assertEquals(personalChannelSettings, PeerNotifySettings.getSettings(channel))

        //если указать персональные настройки для канала с истекшим сроком, то будут возвращаться настройки всех каналов
        val expiredPersonalChannelSettings = NotifySettings(false, Instant.now().minusSeconds(1))
        PeerNotifySettings.updateSettings(channel, expiredPersonalChannelSettings)
        assertEquals(channelSettings, PeerNotifySettings.getSettings(channel))
    }
}
