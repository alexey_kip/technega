package ru.kiporskiy.im.core

import org.junit.jupiter.api.Test
import java.time.Instant

internal class StateTest {

    @Test
    internal fun testTimeDiff() {
        val diff = 1030
        val serverTime = Instant.now().epochSecond + diff

        val state = State()

        state.setServerTimeDiff(serverTime.toInt())

        assert(diff - state.serverTimeDiff <= 2)
    }

    @Test
    internal fun testTimeDiffMinus() {
        val diff = -1030
        val serverTime = Instant.now().epochSecond + diff

        val state = State()

        state.setServerTimeDiff(serverTime.toInt())

        assert(diff - state.serverTimeDiff <= 2)
    }

}
