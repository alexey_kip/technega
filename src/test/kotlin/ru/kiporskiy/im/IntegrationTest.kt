package ru.kiporskiy.im

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

/**
 * Метка для интеграционных тестов
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
@Tag("integration")
@Test
annotation class IntegrationTest