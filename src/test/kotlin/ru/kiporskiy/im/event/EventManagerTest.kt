package ru.kiporskiy.im.event

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.util.concurrent.Executors

internal class EventManagerTest {

    @BeforeEach
    internal fun setUp() {
        EventManager.runParallel = false
    }

    @Test
    internal fun subscribeHandlerAndHandleEventOk() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.subscribe(eventType) { _ -> counter++ }
        EventManager.subscribe(eventType) { _ -> counter++ }

        assertEquals(0, counter)

        EventManager.handleEvent(event)
        assertEquals(2, counter)
        EventManager.handleEvent(event)
        assertEquals(4, counter)
    }

    @Test
    internal fun subscribeListenerAndHandleEventOk() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.subscribe(eventType, object: EventListener<Event>{
            override fun handle(event: Event) {
                counter++
            }
        })
        EventManager.subscribe(eventType, object: EventListener<Event>{
            override fun handle(event: Event) {
                counter++
            }
        })

        assertEquals(0, counter)

        EventManager.handleEvent(event)
        assertEquals(2, counter)
        EventManager.handleEvent(event)
        assertEquals(4, counter)
    }

    @Test
    internal fun unsubscribeHandler() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.subscribe(eventType) { _ -> counter++ }
        EventManager.unsubscribe(eventType)

        assertEquals(0, counter)

        EventManager.handleEvent(event)

        assertEquals(0, counter)
    }

    @Test
    internal fun unsubscribeListeners() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.subscribe(eventType, object: EventListener<Event>{
            override fun handle(event: Event) {
                counter++
            }
        })
        EventManager.unsubscribe(eventType)

        assertEquals(0, counter)

        EventManager.handleEvent(event)

        assertEquals(0, counter)
    }

    @Test
    internal fun handleEvents() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.subscribe(eventType, object: EventListener<Event>{
            override fun handle(event: Event) {
                counter++
            }
        })

        assertEquals(0, counter)

        EventManager.handleEvents(listOf(event, event, event))

        assertEquals(3, counter)
    }

    @Disabled("Долгий тест")
    @Test
    internal fun handleEventsParallel() {
        val eventType = mock<EventType>()
        val event = mock<Event>()
        var counter = 0

        whenever(event.getType()).thenReturn(eventType)

        EventManager.runParallel = true
        EventManager.subscribe(eventType, object: EventListener<Event>{
            override fun handle(event: Event) {
                counter++
            }
        })

        assertEquals(0, counter)

        //для тестов корутины будут выполняться в том же потоке, поэтому создадим отдельный поток
        Executors.newSingleThreadExecutor().run {
            EventManager.handleEvents(listOf(event, event, event))
        }

        Thread.sleep(500)

        assertEquals(3, counter)
    }
}
