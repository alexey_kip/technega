package ru.kiporskiy.im

import io.rsocket.SocketAcceptor
import io.rsocket.core.RSocketServer
import io.rsocket.transport.netty.server.CloseableChannel
import io.rsocket.transport.netty.server.TcpServerTransport
import org.slf4j.LoggerFactory

/**
 * Создание сервера для выполнения интеграционного тестирования запросов
 */
internal class RSocketTestServer(val port: Int, val acceptor: SocketAcceptor) {
    companion object {
        private val log = LoggerFactory.getLogger(RSocketTestServer::class.java)
    }

    var context: CloseableChannel? = null

    fun runServer() {
        context = RSocketServer.create()
            .acceptor(acceptor)
            .bind(TcpServerTransport.create(port))
            .block()
    }

    fun close() {
        context?.dispose()
    }

}
