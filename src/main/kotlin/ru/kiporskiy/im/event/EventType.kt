package ru.kiporskiy.im.event

/**
 * Тип события
 */
interface EventType {

    companion object {

        /**
         * Получить тип события по умолчанию
         */
        fun getDefault(id: String): EventType {
            return DefaultEventType(id)
        }

    }

}

/**
 * Тип события по умолчанию
 */
data class DefaultEventType(val string: String) : EventType
