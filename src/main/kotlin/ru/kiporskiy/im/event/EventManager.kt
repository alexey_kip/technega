package ru.kiporskiy.im.event

import com.google.common.collect.Multimap
import com.google.common.collect.MultimapBuilder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock

/**
 * Класс для управления событиями
 */
object EventManager {

    private val log = LoggerFactory.getLogger(EventManager::class.java)

    /**
     * Запускать обработку событий в отдельных потоках
     */
    var runParallel = true

    /**
     * Обработчики событий
     */
    private val eventHandlers: Multimap<EventType, (Event) -> Unit>

    /**
     * Слушатели событий
     */
    private val eventListeners: Multimap<EventType, EventListener<Event>>

    /**
     * Инструмент для обеспечения безопасной записи и чтения обработчиков событий
     */
    private val lock: ReadWriteLock

    init {
        this.eventHandlers = MultimapBuilder.hashKeys().linkedListValues().build<EventType, (Event) -> Unit>()
        this.eventListeners = MultimapBuilder.hashKeys().linkedListValues().build<EventType, EventListener<Event>>()
        this.lock = ReentrantReadWriteLock(true)
    }


    /**
     * Добавить обработчик события
     */
    fun subscribe(type: EventType, handler: (Event) -> Unit) {
        this.lock.writeLock().lock()
        try {
            this.eventHandlers.put(type, handler)
        } finally {
            this.lock.writeLock().unlock()
        }
    }

    /**
     * Добавить слушатель событий
     */
    fun subscribe(type: EventType, handler: EventListener<Event>) {
        this.lock.writeLock().lock()
        try {
            this.eventListeners.put(type, handler)
        } finally {
            this.lock.writeLock().unlock()
        }
    }

    /**
     * Описать все обработчики и слушатели событий
     */
    fun unsubscribe(type: EventType) {
        this.lock.writeLock().lock()
        try {
            this.eventListeners.removeAll(type)
            this.eventHandlers.removeAll(type)
        } finally {
            this.lock.writeLock().unlock()
        }
    }

    /**
     * Выполнить обработку входящего события
     */
    fun handleEvent(event: Event) {
        this.lock.readLock().lock()
        try {
            val listeners = this.eventListeners.get(event.getType())
            val handlers = this.eventHandlers.get(event.getType())

            //доступен запуск (в отдельных потоках) и в текущем потоке
            if (runParallel) {
                listeners.forEach { this.runParallel(event, it) }
                handlers.forEach { this.runParallel(event, it) }
            } else {
                listeners.forEach { this.run(event, it) }
                handlers.forEach { this.run(event, it) }
            }
        } finally {
            this.lock.readLock().unlock()
        }
    }

    private fun runParallel(event: Event, listener: EventListener<Event>) {
        GlobalScope.launch {
            listener.handle(event)
        }
    }

    private fun runParallel(event: Event, listener: (Event) -> Unit) {
        GlobalScope.launch {
            listener.invoke(event)
        }
    }

    private fun run(event: Event, listener: EventListener<Event>) {
        listener.handle(event)
    }

    private fun run(event: Event, listener: (Event) -> Unit) {
        listener.invoke(event)
    }

    /**
     * Запустить обработчики событий
     */
    fun handleEvents(events: Collection<Event>) {
        events.forEach { this.handleEvent(it) }
    }
}
