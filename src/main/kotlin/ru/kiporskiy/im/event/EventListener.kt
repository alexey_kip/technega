package ru.kiporskiy.im.event

/**
 * Слушатель событий
 */
interface EventListener<T : Event> {

    /**
     * Запустить обработку события
     */
    fun handle(event: T)

}
