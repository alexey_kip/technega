package ru.kiporskiy.im.event

/**
 * Интерфейс для событий
 */
interface Event {

    /**
     * Получить тип события
     */
    fun getType(): EventType

}
