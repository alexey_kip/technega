package ru.kiporskiy.im.help

import java.util.*

/**
 * Класс с языковыми настройками
 */
object LanguageSettings {

    /**
     * Текущая локаль
     */
    var currentLocale = Locale("ru")

    /**
     * Получить строковое описание активной локали
     */
    fun getStringLangCode() = currentLocale.language!!

}
