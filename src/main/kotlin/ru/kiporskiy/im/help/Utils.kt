package ru.kiporskiy.im.help

import org.slf4j.Marker
import org.slf4j.MarkerFactory
import ru.kiporskiy.im.core.help.ClientInfo
import java.time.Instant.ofEpochSecond
import java.time.LocalDateTime
import java.time.ZoneId.systemDefault
import java.util.concurrent.ThreadLocalRandom

/**
 * Утилиты управления временем
 */
object TimeUtils {

    /**
     * Перевести время Unix в секундах в LocalDateTime.
     */
    fun epochSecondToLocalDateTime(epochSecond: Int): LocalDateTime {
        return ofEpochSecond(epochSecond.toLong()).atZone(systemDefault()).toLocalDateTime()
    }

    /**
     * Перевести время в LocalDateTime в секунды Unix.
     */
    fun localDateTimeToEpochSecond(time: LocalDateTime): Int {
        return time.atZone(systemDefault()).toEpochSecond().toInt()
    }
}

/**
 * Функции для генерации случайных чисел
 */
object RandomUtils {

    /**
     * Сгенерировать новое случайное число Integer
     */
    fun nextInt() = ThreadLocalRandom.current().nextInt()

    /**
     * Сгенерировать новое случайное число Integer в пределах от min до max
     */
    fun nextInt(min: Int, max: Int) = ThreadLocalRandom.current().nextInt(min, max)

    /**
     * Сгенерировать новое случайное число Long
     */
    fun nextLong() = ThreadLocalRandom.current().nextLong()

    /**
     * Сгенерировать новое случайное положительное число Integer
     */
    fun nextPositiveInt() = Math.abs(nextInt())

    /**
     * Сгенерировать новое случайное положительное число Long
     */
    fun nextPositiveLong() = Math.abs(nextLong())
}

object Loggers {
    /**
     * Логирование сообщений транспортного урочня
     */
    val TRANSPORT_LOG: Marker = MarkerFactory.getMarker("transport")
}

object Times {

    /**
     * Перевод минут в милисекунды
     */
    val Int.minute get() = this * 60.second

    /**
     * Перевод секунд в милисекунды
     */
    val Int.second get() = this * 1000.millis

    /**
     * Милисекунды int в long
     */
    val Int.millis get() = this.toLong()
}

object SystemUtils {

    fun getClientInfo(): ClientInfo {
        val nameOS = System.getProperty("os.name")
        val versionOS = System.getProperty("os.version")
        val systemLang = System.getProperty("user.language")
        val locale = LanguageSettings.getStringLangCode()
        return ClientInfo(
            API_ID,
            nameOS,
            versionOS,
            APPLICATION_VERSION,
            systemLang,
            APPLICATION_CODE,
            locale
        )
    }

}
