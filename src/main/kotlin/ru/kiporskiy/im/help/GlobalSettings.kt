package ru.kiporskiy.im.help

import java.time.Duration

//-----------------MessageManager-------------------


/**
 * Максимальное количество сообщений, которое можно отправить за раз
 */
const val MAX_SENT_MESSAGES_PORTION_SIZE = 20

/**
 * Задержка до повторной отправки сообщений
 */
val REPEAT_MESSAGES_SEND_DURATION = Duration.ofSeconds(15L)!!

/**
 * Паузы между попытками повторной отправки
 */
val REPEAT_TASK_DURATION = Duration.ofSeconds(5L)!!


//---------------------------------------------------


/**
 * Идентификатор приложения
 */
const val API_ID = 10000

/**
 * Версия приложения
 */
const val APPLICATION_VERSION = "v0.1"

/**
 * TelegramAPI, используемый клиентом
 */
const val API_LAYER_ID = 111

/**
 * Кодовое название клиента
 */
const val APPLICATION_CODE = "technocore"


//---------------------------------------------------
