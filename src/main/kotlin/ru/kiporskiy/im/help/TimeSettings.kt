package ru.kiporskiy.im.help

import java.time.Instant

object TimeSettings {

    /**
     * Разница в сек между временем на сервере и локальным временем
     */
    private var serverTimeDifferenceSec: Long = 0L

    /**
     * Обновить серверное время
     */
    fun updateServerTime(serverTime: Instant) {
        serverTimeDifferenceSec = Instant.now().epochSecond - serverTime.epochSecond
    }

    /**
     * Конвертировать время, полученное во входящем ответе от сервера в текущее
     */
    fun getMyTime(serverObjectTime: Instant) = serverObjectTime.minusSeconds(serverTimeDifferenceSec)

    /**
     * Получить разницу между верменем клиента и сервера
     */
    fun getServerTimeDiffs() = serverTimeDifferenceSec
}