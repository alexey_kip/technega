package ru.kiporskiy.im.messaging.router

import ru.kiporskiy.im.domain.service.PingPongService
import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.dto.tl.TLPing
import ru.kiporskiy.im.messaging.dto.tl.TLPong

/**
 * Реализация обработчика сервисных сообщений
 */
class ServiceMessageRouter(private val service: PingPongService) :
    MessageRouter {

    override fun routing(message: Message): Boolean {
        when (val data = message.data) {
            is TLPing -> {
                service.sendPong(message.id, data.pingId)
                return true
            }
            is TLPong -> {
                service.onPong(data.pingId)
                return true
            }
        }
        return false
    }
}
