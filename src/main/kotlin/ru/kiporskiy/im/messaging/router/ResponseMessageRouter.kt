package ru.kiporskiy.im.messaging.router

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.dto.TLObject
import java.util.*

/**
 * Маршрутизация сообщений от сервера, являющимися ответом на ранее отправленный запрос сервера
 */
class ResponseMessageRouter : MessageRouter {

    companion object {
        private val log = LoggerFactory.getLogger(ResponseMessageRouter::class.java)
    }

    /**
     * Коллекция заданий, запускаемых при получении соответствующего ответа от сервера.
     * Содержит отображения идентификатора на тело задания.
     */
    private val handlers = Collections.synchronizedMap(LinkedHashMap<Long, (TLObject) -> Unit>())

    /**
     * Добавить обработчик на уведомление от сервера, являющееся ответом на запрос клиента
     *
     * @param responseId идентификатор запроса
     * @param action действие, выполняемое при получении ответа на запрос
     */
    fun setResponseHandler(responseId: Long, action: (TLObject) -> Unit) {
        if (log.isDebugEnabled) {
            log.info("Добавление реакции за ответ от сервера. Ид запроса: $responseId")
        }

        this.handlers[responseId] = action
    }

    override fun routing(message: Message): Boolean {
        //обрабатываются только сообщения - ответы
        if (message.data.isResponseTo() != null) {
            val responseId = message.data.isResponseTo()!!
            if (log.isInfoEnabled) {
                log.info("Ответ на запрос $responseId: ${message.data}")
            }
            this.handlers.remove(responseId)?.invoke(message.data)
            return true
        } else {
            return false
        }
    }
}
