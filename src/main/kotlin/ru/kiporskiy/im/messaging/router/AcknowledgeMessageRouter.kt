package ru.kiporskiy.im.messaging.router

import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.MessageManager
import ru.kiporskiy.im.messaging.dto.tl.MsgsAck

/**
 * Суть этого маршрутизатора: для каждого входящего сообщения, если это сообщение требует отправки подтверждения на
 * сервер, полученный идентификатор поставить в очередь на отправку подтверждения о получении на сервер.
 * Этот маршрутизатор ДОЛЖЕН ВСЕГДА ИДТИ ПЕРВЫЙ в списке всех доступных маршрутизаторов!!!
 */
class AcknowledgeMessageRouter(private val msgManager: MessageManager) :
    MessageRouter {

    override fun routing(message: Message): Boolean {
        if (message.ack)
            this.msgManager.sendAck(listOf(message.id))

        if (message.data is MsgsAck) {
            val ack = message.data as MsgsAck
            this.msgManager.ack(ack.msgIds.toSet())
            return true
        }

        return false
    }
}
