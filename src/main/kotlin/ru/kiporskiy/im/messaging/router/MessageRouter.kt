package ru.kiporskiy.im.messaging.router

import ru.kiporskiy.im.messaging.Message

/**
 * Класс для перенаправления оповещения от сервера в нужный обработчик.
 *
 * Каждый роутер контроллирует только один тип сообщений (например, обновления или сервисные сообщения, вроде Pong)
 */
interface MessageRouter {

    /**
     * Попытаться обработать сообщение
     *
     * @return true - сообщение успешно обработано, false - текущий роутер не занимается перенаправлением сообщений
     * данного типа
     */
    fun routing(message: Message): Boolean

}
