package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLRpcError : TLObject {

    override fun isError() = true

    abstract val errorCode: Int

    abstract val errorMessage: String

}

@TLSerialization(constructor = 0x2144ca19, fields = [
    TLField("errorCode", position = 0),
    TLField("errorMessage", position = 1)
])
data class RpcError(override val errorCode: Int, override val errorMessage: String) : TLRpcError()
