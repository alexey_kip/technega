package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLCodeSettings : TLObject

@TLSerialization(
    constructor = 0xdebebe83.toInt(), flagPosition = 0,
    fields = [
        TLField("allowFlashcall", flag = 0),
        TLField("currentNumber", flag = 1),
        TLField("allowAppHash", flag = 4)
    ]
)
data class CodeSettings(
    val allowFlashcall: Boolean,
    val currentNumber: Boolean,
    val allowAppHash: Boolean
) : TLCodeSettings()
