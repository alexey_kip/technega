package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLCodeType: TLObject {

    enum class Type {
        SMS,
        CALL,
        FLASHCALL
    }

    abstract fun getType(): Type
}

@TLSerialization(constructor = 0x72a3158c)
object CodeTypeSms : TLCodeType() {
    override fun getType() = Type.SMS

}

@TLSerialization(constructor = 0x741cd3e3)
object CodeTypeCall : TLCodeType() {
    override fun getType() = Type.CALL
}

@TLSerialization(constructor = 0x226ccefb)
object CodeTypeFlashCall : TLCodeType() {
    override fun getType() = Type.FLASHCALL
}
