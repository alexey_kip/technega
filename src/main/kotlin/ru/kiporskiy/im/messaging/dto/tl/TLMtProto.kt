package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLMtProto : TLObject

sealed class TLMessage : TLMtProto() {
    override fun isMessage() = true

    abstract val id: Long

    abstract val seqNo: Int

    abstract val body: TLObject

}

sealed class TLMessageContainer : TLObject {
    override fun isMessageContainer() = true

    abstract val messages: Array<TLMessage>
}

@TLSerialization(fields = [
    TLField("id", position = 0),
    TLField("seqNo", position = 1),
    TLField("body", position = 2)
], constructor = 0)
data class Message(override val id: Long, override val seqNo: Int, override val body: TLObject) : TLMessage()

@TLSerialization(fields = [TLField("messages", ignoreConstructor = true)], constructor = 0x73f1f8dc)
data class MessageContainer(override val messages: Array<TLMessage>) : TLMessageContainer() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MessageContainer

        if (!messages.contentEquals(other.messages)) return false

        return true
    }

    override fun hashCode(): Int {
        return messages.contentHashCode()
    }
}
