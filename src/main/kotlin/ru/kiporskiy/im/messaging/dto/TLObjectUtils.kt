package ru.kiporskiy.im.messaging.dto

import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.transport.binary.ByteWrapper

/**
 * Утилиты для преобразования сообщения (Message) в байты и обратно
 */
interface TLObjectUtils {

    /**
     * Преобразовать сообщение в массив байтов
     */
    fun serialize(obj: Message): ByteWrapper

    /**
     * Преобразовать сообщения в массив байтов
     */
    fun serialize(vararg obj: Message): ByteWrapper

    /**
     * Преобразовать коллекцию сообщений в массив байтов
     */
    fun serialize(obj: Collection<Message>): ByteWrapper

    /**
     * Преобразовать массив байтов обратно в список сообщений
     */
    fun deserialize(data: ByteWrapper): List<Message>

}
