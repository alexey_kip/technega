package ru.kiporskiy.im.messaging.dto.tl.function

import ru.kiporskiy.im.messaging.dto.TLFunction
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import ru.kiporskiy.im.messaging.dto.tl.TLCodeSettings

/**
 * Запросы для работы с авторизациями
 */
sealed class TLAuth : TLFunction

/**
 * Отправить код для входа в приложение
 */
@TLSerialization(constructor = 0xa677244f.toInt(), fields = [
    TLField("phoneNumber", 1),
    TLField("apiId", 2),
    TLField("apiHash", 3),
    TLField("settings", 4)
])
data class SendCode(
    val phoneNumber: String,
    val apiId: Int,
    val apiHash: String,
    val settings: TLCodeSettings
) : TLAuth()

/**
 * Повторно отправить код для входа в приложение. Должен быть применен другой способ отправки (напирмер, если код был
 * отправлен в смс, то попытаться отправить через звонок)
 */
@TLSerialization(constructor = 0x3ef1a9bf, fields = [
    TLField("phoneNumber", 1),
    TLField("phoneCodeHash", 2)
])
data class ResendCode(
    val phoneNumber: String,
    val phoneCodeHash: String
) : TLAuth()

/**
 * Сбросить отправленный код на сервере
*/
@TLSerialization(constructor = 0x1f040578, fields = [
    TLField("phoneNumber", 1),
    TLField("phoneCodeHash", 2)
])
data class CancelCode(
    val phoneNumber: String,
    val phoneCodeHash: String
) : TLAuth()
