package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLDcOption : TLObject {

    abstract val id: Int

    abstract val mediaOnly: Boolean

    abstract val ipAddress: String

    abstract val port: Int

}

@TLSerialization(constructor = 0x18b7a10d, flagPosition = 0, fields = [
    TLField(name = "ipv6", flag = 0),
    TLField(name = "mediaOnly", flag = 1),
    TLField(name = "tcpOnly", flag = 2),
    TLField(name = "cdn", flag = 3),
    TLField(name = "static", flag = 4),
    TLField(name = "id", position = 1),
    TLField(name = "ipAddress", position = 2),
    TLField(name = "port", position = 3),
    TLField(name = "secret", position = 4, flag = 10)
])
data class DcOption(
        val ipv6: Boolean,
        override val mediaOnly: Boolean,
        val tcpOnly: Boolean,
        val cdn: Boolean,
        val static: Boolean,
        override val id: Int,
        override val ipAddress: String,
        override val port: Int,
        val secret: ByteArray?
) : TLDcOption() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DcOption

        if (ipv6 != other.ipv6) return false
        if (mediaOnly != other.mediaOnly) return false
        if (tcpOnly != other.tcpOnly) return false
        if (cdn != other.cdn) return false
        if (static != other.static) return false
        if (id != other.id) return false
        if (ipAddress != other.ipAddress) return false
        if (port != other.port) return false
        if (secret != null) {
            if (other.secret == null) return false
            if (!secret.contentEquals(other.secret)) return false
        } else if (other.secret != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ipv6.hashCode()
        result = 31 * result + mediaOnly.hashCode()
        result = 31 * result + tcpOnly.hashCode()
        result = 31 * result + cdn.hashCode()
        result = 31 * result + static.hashCode()
        result = 31 * result + id
        result = 31 * result + ipAddress.hashCode()
        result = 31 * result + port
        result = 31 * result + (secret?.contentHashCode() ?: 0)
        return result
    }
}
