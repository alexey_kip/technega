package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLMsgsAck : TLObject {
    companion object {
        fun of(ids: List<Long>): TLMsgsAck {
            return MsgsAck(ids.toLongArray())
        }
    }

    override fun ack() = false
}

@TLSerialization(0x62d6b459, fields = [TLField("msgIds", position = 0)])
data class MsgsAck(val msgIds: LongArray) : TLMsgsAck() {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MsgsAck

        if (!msgIds.contentEquals(other.msgIds)) return false

        return true
    }

    override fun hashCode() = msgIds.contentHashCode()
}
