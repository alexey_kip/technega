package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLSentCode : TLObject {

    abstract val type: TLSentCodeType

    abstract val phoneCodeHash: String

    abstract val nextType: TLCodeType?

    abstract val timeout: Int?

}

@TLSerialization(
    0x5e002502, flagPosition = 0,
    fields = [
        TLField("type", 0),
        TLField("phoneCodeHash", 1),
        TLField("nextType", 2, 1),
        TLField("timeout", 3, 2)
    ]
)
data class SentCode(
    override val type: TLSentCodeType,
    override val phoneCodeHash: String,
    override val nextType: TLCodeType?,
    override val timeout: Int?
): TLSentCode()
