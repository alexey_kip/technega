package ru.kiporskiy.im.messaging.dto

/**
 * Общий интерфейс для всех запросов, отправляемых от клиента к серверу
 */
interface TLFunction : TLObject
