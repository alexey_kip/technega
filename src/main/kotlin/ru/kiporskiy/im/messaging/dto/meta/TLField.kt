package ru.kiporskiy.im.messaging.dto.meta

/**
 * Аннотация, описывающая правила сериализации/десериализации TL объектов
 *
 * @param name название поля
 * @param position порядковый номер поля
 * @param flag номер флага
 * @param ignoreConstructor не упаковывать конструктор
 * @param bytesSize количество байтов (если сериализуется массив байтов, у которого заранее известен размер
 * @param binary отмечает массив байтов, как готовый к отправке бинарный объект (нет ни конструктора, ни других
 * дополнительных параметров)
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class TLField(val name: String,
                         val position: Int = -1,
                         val flag: Int = -1,
                         val ignoreConstructor: Boolean = false,
                         val bytesSize: Int = 0,
                         val binary: Boolean = false)
