package ru.kiporskiy.im.messaging.dto

import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import ru.kiporskiy.im.messaging.dto.tl.*
import ru.kiporskiy.im.messaging.dto.tl.function.TLAuth
import ru.kiporskiy.im.messaging.dto.tl.function.TLHelp
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.primaryConstructor

/**
 * "Словарь" tl клсааов.
 * Осуществляет поиск tl класса по идентификатору.
 */
object TLDictionary {

    private val sealedClasses = listOf(
        TLAuth::class,
        TLBoolean::class,
        TLCodeSettings::class,
        TLCodeType::class,
        TLConfig::class,
        TLDcOption::class,
        TLGzipPackaged::class,
        TLInputContact::class,
        TLInputPeer::class,
        TLInputUser::class,
        TLHelp::class,
        TLMessage::class,
        TLMessageContainer::class,
        TLMsgsAck::class,
        TLPing::class,
        TLPong::class,
        TLRpcError::class,
        TLRpcResult::class,
        TLSentCode::class,
        TLSentCodeType::class
    )

    private val classesMap: Map<Int, KClass<out TLObject>>

    private val paramsMap: HashMap<Int, Map<String, KParameter>>

    init {
        paramsMap = HashMap()
        classesMap = HashMap()

        sealedClasses.forEach {
            it.sealedSubclasses.forEach { subclass ->
                val constructor = subclass.findAnnotation<TLSerialization>()!!.constructor
                classesMap[constructor] = subclass
            }
        }
    }


    fun get(constructor: Int): KClass<out TLObject> = classesMap[constructor]
        ?: error("TLObject с конструктором ${constructor.toString(16)} не найден")

    fun getProperties(constructor: Int): Map<String, KParameter> {
        return paramsMap.getOrPut(constructor) {
            classesMap.get(constructor)?.primaryConstructor!!.parameters.map { it.name!! to it }.toMap()
        }
    }
}
