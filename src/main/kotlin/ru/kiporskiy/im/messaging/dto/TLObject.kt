package ru.kiporskiy.im.messaging.dto

/**
 * Классы для "общения" с сервером
 */
interface TLObject {

    /**
     * Требует ли объект этого класса, полученный от сервера подтверждение о получении
     */
    fun ack(): Boolean = true

    /**
     * Содержит ли объект этого класса ошибку
     */
    fun isError(): Boolean = false

    /**
     * Объект содержит ответ на запрос, отправленный к серверу
     */
    fun isResponseTo(): Long? = null

    /**
     * Является ли объект сжатым представлением
     */
    fun isGzip(): Boolean = false

    /**
     * Является ли объект сообщением
     */
    fun isMessage(): Boolean = false

    /**
     * Является ли объект контейнером сообщений
     */
    fun isMessageContainer(): Boolean = false

    /**
     * Объект является обновлением
     */
    fun isUpdate(): Boolean = false
}
