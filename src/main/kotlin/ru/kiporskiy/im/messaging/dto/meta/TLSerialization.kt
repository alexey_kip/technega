package ru.kiporskiy.im.messaging.dto.meta

/**
 * Правила сериализации класса
 *
 * @param constructor уникальный конструктор
 * @param flagPosition порядковый номер поля - флага (если есть)
 * @param fields правила сериализации полей
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class TLSerialization(val constructor: Int,
                                 val flagPosition: Int = -1,
                                 val fields: Array<TLField> = [])
