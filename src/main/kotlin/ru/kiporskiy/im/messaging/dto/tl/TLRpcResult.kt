package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLRpcResult : TLObject {

    abstract val result: TLObject

    abstract val reqMsgId: Long

}

@TLSerialization(0xf35c6d01.toInt(), fields = [
    TLField("reqMsgId", 0),
    TLField("result", 1)
])
data class RpcResult(override val reqMsgId: Long, override val result: TLObject) : TLRpcResult() {
    override fun isResponseTo() = reqMsgId
}
