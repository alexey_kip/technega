package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import java.lang.IllegalStateException

sealed class TLSentCodeType: TLObject {

    enum class Type {
        SMS,
        APP,
        CALL,
        FLASH
    }

    abstract fun getType(): Type

    abstract val length: Int
}

@TLSerialization(fields = [TLField("length", position = 0)], constructor = 0x3dbb5986)
data class SentCodeTypeApp(override val length: Int): TLSentCodeType() {
    override fun getType() = Type.APP
}

@TLSerialization(fields = [TLField("length", position = 0)], constructor = 0xc000bba2.toInt())
data class SentCodeTypeSms(override val length: Int): TLSentCodeType() {
    override fun getType() = Type.SMS
}

@TLSerialization(fields = [TLField("length", position = 0)], constructor = 0x5353e5a7)
data class SentCodeTypeCall(override val length: Int): TLSentCodeType() {
    override fun getType() = Type.CALL
}

@TLSerialization(fields = [TLField("pattern", position = 0)], constructor = 0x5353e5a7)
data class SentCodeTypeFlashCall(val pattern: String): TLSentCodeType() {
    override fun getType() = Type.FLASH

    override val length: Int
        get() = throw IllegalStateException()
}
