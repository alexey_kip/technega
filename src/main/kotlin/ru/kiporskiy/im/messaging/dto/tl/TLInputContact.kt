package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLInputContact : TLObject

@TLSerialization(0xf392b7f4.toInt(),
        fields = [
            TLField("clientId", 0),
            TLField("phone", 1),
            TLField("firstName", 2),
            TLField("lastName", 3)
        ])
data class InputPhoneContact(val clientId: Long, val phone: String, val firstName: String, val lastName: String) : TLInputContact()
