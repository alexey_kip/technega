package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject

sealed class TLUpdates : TLObject {

    override fun isUpdate() = true

}
