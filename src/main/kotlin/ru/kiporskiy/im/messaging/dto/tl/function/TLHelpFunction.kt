package ru.kiporskiy.im.messaging.dto.tl.function

import ru.kiporskiy.im.messaging.dto.TLFunction
import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

/**
 * Вспомогательные запросы
 */
sealed class TLHelp : TLFunction

/**
 * Запрос конфигов от сервера
 */
@TLSerialization(constructor = 0xc4f9186b.toInt())
object GetConfig : TLHelp()

/**
 * Обертка над запросом, содержащая параметры инициализации соединения с сервером
 */
@TLSerialization(constructor = 0x785188b8, flagPosition = 0, fields = [
    TLField("apiId", 1),
    TLField("deviceModel", 2),
    TLField("systemVersion", 3),
    TLField("appVersion", 4),
    TLField("systemLangCode", 5),
    TLField("langPack", 6),
    TLField("langCode", 7),
    TLField("query", 8)
])
data class InitConnection(
        val apiId: Int,
        val deviceModel: String,
        val systemVersion: String,
        val appVersion: String,
        val systemLangCode: String,
        val langPack: String,
        val langCode: String,
        val query: TLObject) : TLHelp()

/**
 * Обертка над запросом, содержащая версию используемого API
 */
@TLSerialization(constructor = 0xda9b0d0d.toInt(), fields = [
    TLField("apiLayerId", 1),
    TLField("query", 2)
])
data class InvokeWithLayer(
        val apiLayerId: Int,
        val query: TLObject) : TLHelp()
