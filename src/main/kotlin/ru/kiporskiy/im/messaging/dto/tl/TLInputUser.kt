package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLInputUser : TLObject


@TLSerialization(constructor = 0xb98886cf.toInt())
object InputUserEmpty : TLInputUser()


@TLSerialization(constructor = 0xf7c1b13f.toInt())
object InputUserSelf : TLInputUser()


@TLSerialization(constructor = 0xd8292816.toInt(),
        fields = [
            TLField("userId", 0),
            TLField("accessHash", 1)
        ])
data class InputUser(val userId: Int, val accessHash: Long) : TLInputUser()


@TLSerialization(constructor = 0x2d117597,
        fields = [
            TLField("peer", 0),
            TLField("msgId", 1),
            TLField("userId", 2)
        ])
data class InputUserFromMessage(val peer: TLInputPeer, val msgId: Int, val userId: Int) : TLInputUser()
