package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLGzipPackaged : TLObject {
    override fun isGzip() = true
}

@TLSerialization(fields = [TLField("packedData", position = 0)], constructor = 0x3072cfa1)
data class GzipPackaged(val packedData: String) : TLGzipPackaged()
