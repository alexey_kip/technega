package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLPing : TLObject {
    abstract val pingId: Long
}

@TLSerialization(constructor = 0x7abe77ec, fields = [TLField("pingId", 0)])
data class Ping(override val pingId: Long) : TLPing()
