package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import ru.kiporskiy.im.messaging.dto.tl.TLBoolean.Companion.FALSE_CONSTRUCTOR
import ru.kiporskiy.im.messaging.dto.tl.TLBoolean.Companion.TRUE_CONSTRUCTOR

sealed class TLBoolean : TLObject {

    companion object {
        const val TRUE_CONSTRUCTOR = 0x997275b5.toInt()
        const val FALSE_CONSTRUCTOR = 0xbc799737.toInt()

        fun of(bool: Boolean) = if (bool) BooleanTrue else BooleanFalse

        fun of(constructor: Int): TLBoolean {
            if (constructor == TRUE_CONSTRUCTOR)
                return BooleanTrue
            else if (constructor == FALSE_CONSTRUCTOR)
                return BooleanFalse
            else throw UnsupportedOperationException("Unknown constructor: 0x${constructor.toString(16)}")
        }
    }

    abstract fun isTrue(): Boolean
}

@TLSerialization(constructor = TRUE_CONSTRUCTOR)
object BooleanTrue : TLBoolean() {
    override fun isTrue() = true
}

@TLSerialization(constructor = FALSE_CONSTRUCTOR)
object BooleanFalse : TLBoolean() {
    override fun isTrue() = false
}
