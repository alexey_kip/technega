package ru.kiporskiy.im.messaging.dto

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.help.TimeUtils
import ru.kiporskiy.im.messaging.IdGenerator
import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import ru.kiporskiy.im.messaging.dto.tl.MessageContainer
import ru.kiporskiy.im.messaging.dto.tl.TLBoolean
import ru.kiporskiy.im.messaging.dto.tl.TLMessage
import ru.kiporskiy.im.messaging.dto.tl.TLMessageContainer
import ru.kiporskiy.im.transport.binary.ByteWrapper
import java.time.Instant
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.LinkedHashMap
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

/**
 * Реализация интерфейса утилит для преобразования сообщения (Message) в байты и обратно.
 * @see https://core.telegram.org/mtproto/serialize
 */
class TLObjectUtilsImpl : TLObjectUtils {

    companion object {
        const val TLVECTOR_CONSTRUCTOR: Int = 0x1cb5c415
        private val log = LoggerFactory.getLogger(TLObjectUtilsImpl::class.java)
    }

    override fun serialize(obj: Message): ByteWrapper {
        val result = extractMessages(obj.data)
        return this.serialize(result)
    }

    private fun extractMessages(obj: TLObject): ru.kiporskiy.im.messaging.dto.tl.Message {
        val messageId = IdGenerator.getId()
        val seqNo = IdGenerator.getSeqNo(obj.ack())
        return ru.kiporskiy.im.messaging.dto.tl.Message(messageId, seqNo, obj)
    }

    override fun serialize(vararg obj: Message): ByteWrapper {
        val messagesArray: Array<TLMessage> = Array(obj.size) { k -> extractMessages(obj[k].data) }
        val container = MessageContainer(messagesArray)
        val result = extractMessages(container)
        return this.serialize(result)
    }

    override fun serialize(obj: Collection<Message>): ByteWrapper {
        val iterator = obj.iterator()
        val messagesArray: Array<TLMessage> = Array(obj.size) { extractMessages(iterator.next().data) }
        val container = MessageContainer(messagesArray)
        val result = extractMessages(container)
        return this.serialize(result)
    }

    fun serialize(obj: TLObject): ByteWrapper {
        val result = ByteWrapper.getDefault()

        //описание правил сериализации класса
        val meta = obj::class.findAnnotation<TLSerialization>()!!

        //запись конструктора
        if (meta.constructor != 0) {
            result.write(meta.constructor)
        }

        //описание свойств объекта
        val properties = obj::class.memberProperties.map { it.name to it }.toMap()

        //построение флагов
        var flags = -1
        if (meta.flagPosition >= 0) {
            flags = buildFlags(obj, properties, meta)
        }

        //если полей нет вообще, либо если есть только флаги
        if (meta.fields.isEmpty() && flags > -1) {
            result.write(flags)
        }

        meta.fields
                .sortedBy { it.position }
                .forEachIndexed { index, tlField ->
                    //если дошли до позиции с флагами
                    if (index == meta.flagPosition) {
                        result.write(flags)
                    }

                    //само поле
                    val field = properties[tlField.name] ?: error("Property ${tlField.name} not found")

                    //значение поля
                    val propertyValue = field.getter.call(obj) ?: return@forEachIndexed

                    //запись значения поля
                    writeValue(tlField, propertyValue, result)
                }

        return result
    }

    private fun buildFlags(obj: TLObject, properties: Map<String, KProperty1<out TLObject, Any?>>, meta: TLSerialization): Int {
        var result = 0

        val nulls = properties.values
                .filter {
                    val field = it.getter.call(obj)
                    return@filter field == null || (field is Boolean && !field)
                }
                .map { it.name }
                .toHashSet()

        meta.fields
                .filter { it.flag >= 0 }
                .forEach { result = setFlag(result, it.flag, !nulls.contains(it.name)) }

        return result
    }

    /**
     * Установить флаг.
     *
     * @param flagsField поле с флагами.
     * @param bitNumber  позиция бита, которую нужно установить
     * @param value      значение (true - установить, false - снять).
     * @return поле с флагами, в котором установлен/снаят требуемый флаг.
     */
    private fun setFlag(flagsField: Int, bitNumber: Int, value: Boolean): Int {
        if (value) {
            return flagsField or (1 shl bitNumber)
        } else {
            return flagsField and (1 shl bitNumber).inv()
        }
    }

    private fun writeValue(tlField: TLField, propertyValue: Any, buffer: ByteWrapper) {
        when (propertyValue) {
            is Int -> buffer.write(propertyValue)
            is Long -> buffer.write(propertyValue)
            is Double -> buffer.write(propertyValue)
            is String -> buffer.write(propertyValue)
            is ByteArray -> buffer.write(propertyValue, tlField.binary || tlField.bytesSize > 0)
            is IntArray -> {
                if (!tlField.ignoreConstructor) {
                    buffer.write(TLVECTOR_CONSTRUCTOR)
                }
                buffer.write(propertyValue.size)
                propertyValue.forEach { buffer.write(it) }
            }
            is LongArray -> {
                if (!tlField.ignoreConstructor) {
                    buffer.write(TLVECTOR_CONSTRUCTOR)
                }
                buffer.write(propertyValue.size)
                propertyValue.forEach { buffer.write(it) }
            }
            is DoubleArray -> {
                if (!tlField.ignoreConstructor) {
                    buffer.write(TLVECTOR_CONSTRUCTOR)
                }
                buffer.write(propertyValue.size)
                propertyValue.forEach { buffer.write(it) }
            }
            is Array<*> -> {
                if (!tlField.ignoreConstructor) {
                    buffer.write(TLVECTOR_CONSTRUCTOR)
                }
                buffer.write(propertyValue.size)
                propertyValue.javaClass.componentType
                propertyValue.forEach { writeValue(tlField, it!!, buffer) }
            }
            is TLObject -> {
                buffer.write(serialize(propertyValue))
            }
            is LocalDateTime -> {
                buffer.write(TimeUtils.localDateTimeToEpochSecond(propertyValue))
            }
            is Instant -> {
                buffer.write(propertyValue.epochSecond.toInt())
            }
            is Boolean -> {
                if (tlField.flag == -1) {
                    buffer.write(serialize(TLBoolean.of(propertyValue)))
                }
            }
            else -> throw UnsupportedOperationException()
        }
    }

    fun deserializeData(messageData: ByteWrapper): TLObject {
        //ид конструктора
        val constructor = messageData.readInt()

        //десериализация объекта
        return deserializeObject(messageData, constructor)
    }

    fun deserializeObject(messageData: ByteWrapper, constructor: Int = 0): TLObject {
        //ссылка на класс
        val clazz = TLDictionary.get(constructor)

        //если синглтон - вернуть результат сразу
        if (clazz.objectInstance != null) {
            return clazz.objectInstance!!
        }

        //все свойства объекта
        val props = TLDictionary.getProperties(constructor)

        //описание правил сериализации
        val classMeta = clazz.findAnnotation<TLSerialization>()!!

        //значение свойств объекта
        val propsValues = readParams(messageData, props, classMeta)

        //создание экземпляра класса
        return clazz.primaryConstructor!!.callBy(propsValues)
    }

    fun deserializeMessage(messageData: ByteWrapper) = deserializeObject(messageData, 0)

    /**
     * Прочитать значения всех параметров класса
     */
    private fun readParams(data: ByteWrapper,
                           props: Map<String, KParameter>,
                           meta: TLSerialization): Map<KParameter, Any?> {
        //накопленный результат
        val result = LinkedHashMap<KParameter, Any?>()

        //флаги
        var flags = BitSet()

        //если флаг идет самым первым - то прочитать сразу
        if (meta.flagPosition == 0) {
            flags = readFlags(data)
        }

        //чтение каждого парамтера
        meta.fields
                .sortedBy { it.position }
                .forEachIndexed { index, fieldMeta ->
                    //если индекс поля совпадает с позицией флага - прочитать флаг
                    if (meta.flagPosition > 0 && index == meta.flagPosition) {
                        flags = readFlags(data)
                    }

                    //получение параметра
                    val param = props[fieldMeta.name]!!

                    //если флаг - то ничего читать не надо
                    if (fieldMeta.flag > -1 && param.type.classifier == Boolean::class && !param.type.isMarkedNullable) {
                        val value = flags[fieldMeta.flag]
                        result[param] = value
                        log.info("Read #$index field ${fieldMeta.name} type: Boolean")
                    } else
                    // если поле помечено флагом, значение которого false - не требуется десериализовать поле
                        if (fieldMeta.flag > -1 && !flags[fieldMeta.flag]) {
                            assert(param.type.isMarkedNullable) { "Flagged type could be marked nullable: $param" }
                            result[param] = null
                            log.info("Read #$index field ${fieldMeta.name} type: Null")
                        } else {
                            result[param] = readValue(data, fieldMeta, param.type.classifier as KClass<*>)
                            log.info("Read #$index field ${fieldMeta.name} type: {}", result[param]?.javaClass?.name)
                        }
                }
        return result
    }

    private fun readFlags(data: ByteWrapper): BitSet {
        val flags = data.readInt()
        val result = BitSet(Int.SIZE_BITS)
        for (i in 0 until Int.SIZE_BITS) {
            result[i] = flags and (1 shl i) != 0
        }
        return result
    }

    private fun readValue(data: ByteWrapper, meta: TLField, clazz: KClass<*>): Any {
        //вложенный TLObject
        if (clazz.isSubclassOf(TLObject::class)) {
            return this.deserializeData(data)
        }

        //массив объектов
        if (Array<Any>::class.java.isAssignableFrom(clazz.java)
                || clazz == ByteArray::class
                || clazz == IntArray::class
                || clazz == LongArray::class
                || clazz == DoubleArray::class) {
            return readArray(data, meta, clazz)
        }

        return when (clazz) {
            Int::class -> data.readInt()
            Long::class -> data.readLong()
            Double::class -> data.readDouble()
            String::class -> data.readString()
            Boolean::class -> TLBoolean.of(data.readInt())
            LocalDateTime::class -> TimeUtils.epochSecondToLocalDateTime(data.readInt())
            Instant::class -> Instant.ofEpochSecond(data.readInt().toLong())
            else -> throw UnsupportedOperationException("$clazz")
        }
    }

    private fun readArray(data: ByteWrapper, meta: TLField, clazz: KClass<*>): Any {
        val result: Any

        if (!meta.ignoreConstructor && clazz != ByteArray::class) {
            val arrayConstructor = data.readInt()
            assert(arrayConstructor == TLVECTOR_CONSTRUCTOR) { "Constructor is not vector: $arrayConstructor" }
        }

        if (clazz == ByteArray::class) {
            return data.readBytes(meta.binary, meta.bytesSize)
        }

        //количество элементов в массиве
        val length = data.readInt()

        //массив TLObject
        if (Array<TLObject>::class.java.isAssignableFrom(clazz.java)) {
            val isMessages = clazz.java.componentType.isAssignableFrom(ru.kiporskiy.im.messaging.dto.tl.Message::class.java)
            @Suppress("UNCHECKED_CAST")
            result = java.lang.reflect.Array.newInstance(clazz.java.componentType, length) as Array<Any>
            for (i in 0 until length) {
                if (isMessages) {
                    result[i] = deserializeMessage(data)
                } else {
                    result[i] = deserializeData(data)
                }
            }
            return result
        }

        when (clazz) {
            IntArray::class -> {
                result = IntArray(length) { data.readInt() }
                return result
            }
            Array<Int>::class -> {
                result = Array(length) { data.readInt() }
                return result
            }
            LongArray::class -> {
                result = LongArray(length) { data.readLong() }
                return result
            }
            Array<Long>::class -> {
                result = Array(length) { data.readLong() }
                return result
            }
            DoubleArray::class -> {
                result = DoubleArray(length) { data.readDouble() }
                return result
            }
            Array<Double>::class -> {
                result = Array(length) { data.readDouble() }
                return result
            }
            Array<String>::class -> {
                result = Array(length) { data.readString() }
                return result
            }
            else -> throw UnsupportedOperationException(clazz.toString())
        }

    }

    override fun deserialize(data: ByteWrapper): List<Message> {
        if (data.size() == 0) {
            return listOf()
        }

        val message = this.deserializeMessage(data)

        require(message.isMessage())

        val tlMessage = message as TLMessage
        if (tlMessage.body.isMessageContainer()) {
            val container = tlMessage.body as TLMessageContainer
            return container.messages.map { Message.of(it.body, it.id, it.seqNo % 2 == 0) }
        } else {
            return listOf(Message.of(tlMessage.body, tlMessage.id, tlMessage.seqNo % 2 == 0))
        }
    }
}

