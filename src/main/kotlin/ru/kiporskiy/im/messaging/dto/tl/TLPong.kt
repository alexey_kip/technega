package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLPong : TLObject {
    abstract val pingId: Long
}

@TLSerialization(constructor = 0x347773c5, fields = [
    TLField("msgId", 1),
    TLField("pingId", 0)
])
data class Pong(val msgId: Long, override val pingId: Long) : TLPong()
