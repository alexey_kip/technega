package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization
import java.time.Instant
import java.time.LocalDateTime

sealed class TLConfig : TLObject {

    abstract val date: Instant

    abstract val dcOptions: Array<TLDcOption>

}

@TLSerialization(0x330b4067, flagPosition = 0, fields = [
    //flags
    TLField("phoneCallsEnabled", 0, 1),
    TLField("defaultP2pContacts", 0, 3),
    TLField("preloadFeaturedStickers", 0, 4),
    TLField("ignorePhoneEntities", 0, 5),
    TLField("revokePmInbox", 0, 6),
    TLField("blockedMode", 0, 8),
    TLField("pfsEnabled", 0, 13),
    //properties
    TLField("date", 1),
    TLField("expires", 2),
    TLField("testMode", 3),
    TLField("thisDc", 4),
    TLField("dcOptions", 5),
    TLField("dcTxtDomainName", 6),
    TLField("chatSizeMax", 7),
    TLField("megagroupSizeMax", 8),
    TLField("forwardedCountMax", 9),
    TLField("onlineUpdatePeriodMs", 10),
    TLField("offlineBlurTimeoutMs", 11),
    TLField("offlineIdleTimeoutMs", 12),
    TLField("onlineCloudTimeoutMs", 13),
    TLField("notifyCloudDelayMs", 14),
    TLField("notifyDefaultDelayMs", 15),
    TLField("pushChatPeriodMs", 16),
    TLField("pushChatLimit", 17),
    TLField("savedGifsLimit", 18),
    TLField("editTimeLimit", 19),
    TLField("revokeTimeLimit", 20),
    TLField("revokePmTimeLimit", 21),
    TLField("ratingEDecay", 22),
    TLField("stickersRecentLimit", 23),
    TLField("stickersFavedLimit", 24),
    TLField("channelsReadMediaPeriod", 25),
    TLField("tmpSessions", 26, 0),
    TLField("pinnedDialogsCountMax", 27),
    TLField("pinnedInFolderDialogsCountMax", 28),
    TLField("callReceiveTimeoutMs", 29),
    TLField("callRingTimeoutMs", 30),
    TLField("callConnectTimeoutMs", 31),
    TLField("callPacketTimeoutMs", 32),
    TLField("meUrlPerfix", 33),
    TLField("captionLengthMax", 39),
    TLField("messageLengthMax", 40),
    TLField("webfileDcId", 41),
    //properties_flags
    TLField("autoupdateUrlPrefix", 34, 7),
    TLField("gifSearchUsername", 35, 9),
    TLField("venueSearchUsername", 36, 10),
    TLField("imgSearchUsername", 37, 11),
    TLField("staticMapsProvider", 38, 12),
    TLField("suggestedLangCode", 42, 2),
    TLField("langPackVersion", 43, 2),
    TLField("baseLangPackVersion", 44, 2)
])
data class Config(
        val phoneCallsEnabled: Boolean,
        val defaultP2pContacts: Boolean,
        val preloadFeaturedStickers: Boolean,
        val ignorePhoneEntities: Boolean,
        val revokePmInbox: Boolean,
        val blockedMode: Boolean,
        val pfsEnabled: Boolean,
        override val date: Instant,
        val expires: LocalDateTime,
        val testMode: TLBoolean,
        val thisDc: Int,
        override val dcOptions: Array<TLDcOption>,
        val dcTxtDomainName: String,
        val chatSizeMax: Int,
        val megagroupSizeMax: Int,
        val forwardedCountMax: Int,
        val onlineUpdatePeriodMs: Int,
        val offlineBlurTimeoutMs: Int,
        val offlineIdleTimeoutMs: Int,
        val onlineCloudTimeoutMs: Int,
        val notifyCloudDelayMs: Int,
        val notifyDefaultDelayMs: Int,
        val pushChatPeriodMs: Int,
        val pushChatLimit: Int,
        val savedGifsLimit: Int,
        val editTimeLimit: Int,
        val revokeTimeLimit: Int,
        val revokePmTimeLimit: Int,
        val ratingEDecay: Int,
        val stickersRecentLimit: Int,
        val stickersFavedLimit: Int,
        val channelsReadMediaPeriod: Int,
        val tmpSessions: Int?,
        val pinnedDialogsCountMax: Int,
        val pinnedInFolderDialogsCountMax: Int,
        val callReceiveTimeoutMs: Int,
        val callRingTimeoutMs: Int,
        val callConnectTimeoutMs: Int,
        val callPacketTimeoutMs: Int,
        val meUrlPerfix: String,
        val autoupdateUrlPrefix: String?,
        val gifSearchUsername: String?,
        val venueSearchUsername: String?,
        val imgSearchUsername: String?,
        val staticMapsProvider: String?,
        val captionLengthMax: Int,
        val messageLengthMax: Int,
        val webfileDcId: Int,
        val suggestedLangCode: String?,
        val langPackVersion: String?,
        val baseLangPackVersion: String?
) : TLConfig() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Config

        if (phoneCallsEnabled != other.phoneCallsEnabled) return false
        if (defaultP2pContacts != other.defaultP2pContacts) return false
        if (preloadFeaturedStickers != other.preloadFeaturedStickers) return false
        if (ignorePhoneEntities != other.ignorePhoneEntities) return false
        if (revokePmInbox != other.revokePmInbox) return false
        if (blockedMode != other.blockedMode) return false
        if (pfsEnabled != other.pfsEnabled) return false
        if (date != other.date) return false
        if (expires != other.expires) return false
        if (testMode != other.testMode) return false
        if (thisDc != other.thisDc) return false
        if (!dcOptions.contentEquals(other.dcOptions)) return false
        if (dcTxtDomainName != other.dcTxtDomainName) return false
        if (chatSizeMax != other.chatSizeMax) return false
        if (megagroupSizeMax != other.megagroupSizeMax) return false
        if (forwardedCountMax != other.forwardedCountMax) return false
        if (onlineUpdatePeriodMs != other.onlineUpdatePeriodMs) return false
        if (offlineBlurTimeoutMs != other.offlineBlurTimeoutMs) return false
        if (offlineIdleTimeoutMs != other.offlineIdleTimeoutMs) return false
        if (onlineCloudTimeoutMs != other.onlineCloudTimeoutMs) return false
        if (notifyCloudDelayMs != other.notifyCloudDelayMs) return false
        if (notifyDefaultDelayMs != other.notifyDefaultDelayMs) return false
        if (pushChatPeriodMs != other.pushChatPeriodMs) return false
        if (pushChatLimit != other.pushChatLimit) return false
        if (savedGifsLimit != other.savedGifsLimit) return false
        if (editTimeLimit != other.editTimeLimit) return false
        if (revokeTimeLimit != other.revokeTimeLimit) return false
        if (revokePmTimeLimit != other.revokePmTimeLimit) return false
        if (ratingEDecay != other.ratingEDecay) return false
        if (stickersRecentLimit != other.stickersRecentLimit) return false
        if (stickersFavedLimit != other.stickersFavedLimit) return false
        if (channelsReadMediaPeriod != other.channelsReadMediaPeriod) return false
        if (tmpSessions != other.tmpSessions) return false
        if (pinnedDialogsCountMax != other.pinnedDialogsCountMax) return false
        if (pinnedInFolderDialogsCountMax != other.pinnedInFolderDialogsCountMax) return false
        if (callReceiveTimeoutMs != other.callReceiveTimeoutMs) return false
        if (callRingTimeoutMs != other.callRingTimeoutMs) return false
        if (callConnectTimeoutMs != other.callConnectTimeoutMs) return false
        if (callPacketTimeoutMs != other.callPacketTimeoutMs) return false
        if (meUrlPerfix != other.meUrlPerfix) return false
        if (autoupdateUrlPrefix != other.autoupdateUrlPrefix) return false
        if (gifSearchUsername != other.gifSearchUsername) return false
        if (venueSearchUsername != other.venueSearchUsername) return false
        if (imgSearchUsername != other.imgSearchUsername) return false
        if (staticMapsProvider != other.staticMapsProvider) return false
        if (captionLengthMax != other.captionLengthMax) return false
        if (messageLengthMax != other.messageLengthMax) return false
        if (webfileDcId != other.webfileDcId) return false
        if (suggestedLangCode != other.suggestedLangCode) return false
        if (langPackVersion != other.langPackVersion) return false
        if (baseLangPackVersion != other.baseLangPackVersion) return false

        return true
    }

    override fun hashCode(): Int {
        var result = phoneCallsEnabled.hashCode()
        result = 31 * result + defaultP2pContacts.hashCode()
        result = 31 * result + preloadFeaturedStickers.hashCode()
        result = 31 * result + ignorePhoneEntities.hashCode()
        result = 31 * result + revokePmInbox.hashCode()
        result = 31 * result + blockedMode.hashCode()
        result = 31 * result + pfsEnabled.hashCode()
        result = 31 * result + date.hashCode()
        result = 31 * result + expires.hashCode()
        result = 31 * result + testMode.hashCode()
        result = 31 * result + thisDc
        result = 31 * result + dcOptions.contentHashCode()
        result = 31 * result + dcTxtDomainName.hashCode()
        result = 31 * result + chatSizeMax
        result = 31 * result + megagroupSizeMax
        result = 31 * result + forwardedCountMax
        result = 31 * result + onlineUpdatePeriodMs
        result = 31 * result + offlineBlurTimeoutMs
        result = 31 * result + offlineIdleTimeoutMs
        result = 31 * result + onlineCloudTimeoutMs
        result = 31 * result + notifyCloudDelayMs
        result = 31 * result + notifyDefaultDelayMs
        result = 31 * result + pushChatPeriodMs
        result = 31 * result + pushChatLimit
        result = 31 * result + savedGifsLimit
        result = 31 * result + editTimeLimit
        result = 31 * result + revokeTimeLimit
        result = 31 * result + revokePmTimeLimit
        result = 31 * result + ratingEDecay
        result = 31 * result + stickersRecentLimit
        result = 31 * result + stickersFavedLimit
        result = 31 * result + channelsReadMediaPeriod
        result = 31 * result + (tmpSessions ?: 0)
        result = 31 * result + pinnedDialogsCountMax
        result = 31 * result + pinnedInFolderDialogsCountMax
        result = 31 * result + callReceiveTimeoutMs
        result = 31 * result + callRingTimeoutMs
        result = 31 * result + callConnectTimeoutMs
        result = 31 * result + callPacketTimeoutMs
        result = 31 * result + meUrlPerfix.hashCode()
        result = 31 * result + (autoupdateUrlPrefix?.hashCode() ?: 0)
        result = 31 * result + (gifSearchUsername?.hashCode() ?: 0)
        result = 31 * result + (venueSearchUsername?.hashCode() ?: 0)
        result = 31 * result + (imgSearchUsername?.hashCode() ?: 0)
        result = 31 * result + (staticMapsProvider?.hashCode() ?: 0)
        result = 31 * result + captionLengthMax
        result = 31 * result + messageLengthMax
        result = 31 * result + webfileDcId
        result = 31 * result + (suggestedLangCode?.hashCode() ?: 0)
        result = 31 * result + (langPackVersion?.hashCode() ?: 0)
        result = 31 * result + (baseLangPackVersion?.hashCode() ?: 0)
        return result
    }
}
