package ru.kiporskiy.im.messaging.dto.tl

import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.meta.TLField
import ru.kiporskiy.im.messaging.dto.meta.TLSerialization

sealed class TLInputPeer : TLObject


@TLSerialization(constructor = 0x20adaef8,
        fields = [
            TLField("channelId", 0),
            TLField("accessHash", 1)
        ])
data class InputPeerChannel(val channelId: Int, val accessHash: Long) : TLInputPeer()


@TLSerialization(constructor = 0x9c95f7bb.toInt(),
        fields = [
            TLField("peer", 0),
            TLField("msgId", 1),
            TLField("channelId", 2)
        ])
data class InputPeerChannelFromMessage(val peer: TLInputPeer, val msgId: Int, val channelId: Int) : TLInputPeer()


@TLSerialization(constructor = 0x179be863, fields = [TLField("chatId", 0)])
data class InputPeerChat(val chatId: Int) : TLInputPeer()


@TLSerialization(constructor = 0x7f3b18ea)
object InputPeerEmpty : TLInputPeer()


@TLSerialization(constructor = 0x7da07ec9)
object InputPeerSelf : TLInputPeer()


@TLSerialization(constructor = 0x7b8e7de6,
        fields = [
            TLField("userId", 0),
            TLField("accessHash", 1)
        ])
data class InputPeerUser(val userId: Int, val accessHash: Long) : TLInputPeer()


@TLSerialization(constructor = 0x17bae2e6,
        fields = [
            TLField("peer", 0),
            TLField("msgId", 1),
            TLField("userId", 2)
        ])
data class InputPeerUserFromMessage(val peer: TLInputPeer, val msgId: Int, val userId: Int) : TLInputPeer()
