package ru.kiporskiy.im.messaging

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

/**
 * Генератор идентификаторов сообщений и номера последовательности запроса
 */
object IdGenerator {

    /**
     * Последний сгенерированный ид
     */
    private var lastId = AtomicLong(System.currentTimeMillis())

    /**
     * Последний номер в последовательности
     */
    private var lastSeqNo = AtomicInteger(0)

    /**
     * Получить идентификатор сообщения.
     * Зависит от времени
     */
    fun getId() = lastId.incrementAndGet()

    /**
     * Получить номер в последовательности.
     * Зависит от того, требуется ли подтверждение получения запроса от сервера. Если да, то seqNum будет четный,
     * иначе - нечетный
     */
    fun getSeqNo(ack: Boolean = true): Int {
        var result: Int
        if (ack) {
            do {
                result = lastSeqNo.incrementAndGet()
            } while (result % 2 != 0)
        } else {
            do {
                result = lastSeqNo.incrementAndGet()
            } while (result % 2 == 0)
        }
        return result
    }

}
