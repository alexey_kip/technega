package ru.kiporskiy.im.messaging.mediator.converter

import ru.kiporskiy.im.core.auth.*
import ru.kiporskiy.im.core.auth.SentCode
import ru.kiporskiy.im.core.help.Config
import ru.kiporskiy.im.core.help.Dc
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.messaging.dto.tl.*
import java.lang.UnsupportedOperationException

/**
 * Преобразование между DTO объектами и объектами классов для работы бизнес логики
 */
object TLConverter {

    fun error(tlRpcError: TLRpcError) = Error(tlRpcError.errorCode, tlRpcError.errorMessage)

    fun boolean(tlBoolean: TLBoolean) = tlBoolean.isTrue()

    fun config(tlConfig: TLConfig): Config {
        //серверное время
        val serverDateTime = tlConfig.date

        //список датацентров
        val dcList = tlConfig.dcOptions.map { dc -> Dc(dc.id, dc.ipAddress, dc.port, dc.mediaOnly) }

        return Config(serverDateTime, dcList)
    }

    fun sentCode(sentCode: TLSentCode): SentCode {
        //способ, которым было отправлено смс сообщение
        val codeType =
            when (sentCode.type.getType()) {
                TLSentCodeType.Type.APP -> CodeType.APP
                TLSentCodeType.Type.SMS -> CodeType.SMS
                TLSentCodeType.Type.CALL -> CodeType.CALL
                TLSentCodeType.Type.FLASH -> throw UnsupportedOperationException()
            }

        //длина кода
        val length = sentCode.type.length

        //метод, которым СМС будет отправлено в следующий раз
        val nextCodeType = sentCode.nextType?.let {
            when (it.getType()) {
                TLCodeType.Type.SMS -> CodeType.APP
                TLCodeType.Type.CALL -> CodeType.SMS
                else -> throw UnsupportedOperationException()
            }
        }

        return SentCode(codeType, length, sentCode.phoneCodeHash, nextCodeType, sentCode.timeout)
    }
}
