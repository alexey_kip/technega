package ru.kiporskiy.im.messaging.mediator

import ru.kiporskiy.im.core.auth.SentCode
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.AuthMediator
import ru.kiporskiy.im.help.API_ID
import ru.kiporskiy.im.messaging.MessageDispatcher
import ru.kiporskiy.im.messaging.dto.TLFunction
import ru.kiporskiy.im.messaging.mediator.converter.TLConverter
import ru.kiporskiy.im.messaging.dto.tl.CodeSettings
import ru.kiporskiy.im.messaging.dto.tl.TLBoolean
import ru.kiporskiy.im.messaging.dto.tl.TLRpcError
import ru.kiporskiy.im.messaging.dto.tl.TLSentCode
import ru.kiporskiy.im.messaging.dto.tl.function.CancelCode
import ru.kiporskiy.im.messaging.dto.tl.function.ResendCode
import ru.kiporskiy.im.messaging.dto.tl.function.SendCode

class MessagingAuthMediator(private val dispatcher: MessageDispatcher) : AuthMediator {

    /**
     * Запрос на отправку кода для входа
     */
    override fun sendCode(
        phoneNumber: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    ) {
        val settings = CodeSettings(false, false, false)
        val request = SendCode(phoneNumber, API_ID, "", settings)
        this.sendRequestToServer(request, error, result)
    }

    /**
     * Запрос на повторную отправку кода для входа
     */
    override fun resendCode(
        phoneNumber: String,
        phoneCodeHash: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    ) {
        val request = ResendCode(phoneNumber, phoneCodeHash)
        this.sendRequestToServer(request, error, result)
    }

    private fun sendRequestToServer(
        request: TLFunction,
        error: (error: Error) -> Unit,
        result: (sentCodeResult: SentCode) -> Unit
    ) {
        this.dispatcher.sendRequestToServer(request) {
            if (it.isError()) {
                val responseErrorTl = it as TLRpcError
                val responseError = TLConverter.error(responseErrorTl)
                error.invoke(responseError)
            } else {
                assert(it is TLSentCode)
                val responseTl = it as TLSentCode
                val response = TLConverter.sentCode(responseTl)
                result.invoke(response)
            }
        }
    }

    override fun cancelCode(
        phoneNumber: String,
        phoneCodeHash: String,
        result: (codeCancelled: Boolean) -> Unit,
        error: (error: Error) -> Unit
    ) {
        val request = CancelCode(phoneNumber, phoneCodeHash)
        this.dispatcher.sendRequestToServer(request) {
            if (it.isError()) {
                val responseErrorTl = it as TLRpcError
                val responseError = TLConverter.error(responseErrorTl)
                error.invoke(responseError)
            } else {
                assert(it is TLBoolean)
                val responseTl = it as TLBoolean
                val response = TLConverter.boolean(responseTl)
                result.invoke(response)
            }
        }
    }
}
