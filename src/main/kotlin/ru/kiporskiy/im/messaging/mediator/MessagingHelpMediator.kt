package ru.kiporskiy.im.messaging.mediator

import ru.kiporskiy.im.core.help.Config
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.HelpMediator
import ru.kiporskiy.im.help.API_LAYER_ID
import ru.kiporskiy.im.help.SystemUtils
import ru.kiporskiy.im.messaging.MessageDispatcher
import ru.kiporskiy.im.messaging.mediator.converter.TLConverter
import ru.kiporskiy.im.messaging.dto.tl.Ping
import ru.kiporskiy.im.messaging.dto.tl.Pong
import ru.kiporskiy.im.messaging.dto.tl.TLConfig
import ru.kiporskiy.im.messaging.dto.tl.TLRpcError
import ru.kiporskiy.im.messaging.dto.tl.function.GetConfig
import ru.kiporskiy.im.messaging.dto.tl.function.InitConnection
import ru.kiporskiy.im.messaging.dto.tl.function.InvokeWithLayer
import ru.kiporskiy.im.transport.DcAddress
import ru.kiporskiy.im.transport.ServersManager

/**
 * Реализация диспетчера серисных сообщений
 */
class MessagingHelpMediator(private val dispatcher: MessageDispatcher) :
    HelpMediator {

    /**
     * Отправка команды пинг
     */
    override fun runPing(id: Long) {
        val request = Ping(id)
        this.dispatcher.sendRequestToServer(request)
    }

    /**
     * Отправка команды понг (по сути - ответ на пинг, но не оборачивается в RpcResponse)
     */
    override fun runPong(messageId: Long, id: Long) {
        val request = Pong(messageId, id)
        this.dispatcher.sendRequestToServer(request)
    }

    /**
     * Получение конфигов от сервера
     */
    override fun getConfig(serviceInfoRequired: Boolean,
                           result: (configResult: Config) -> Unit,
                           error: (error: Error) -> Unit) {
        val request = if (serviceInfoRequired) {
            val systemConfig = SystemUtils.getClientInfo()
            val layerQuery = InvokeWithLayer(API_LAYER_ID, GetConfig)
            with(systemConfig) {
                InitConnection(appId, deviceModel, systemVersion,
                    appVersion, systemLangCode, langPack,
                    currentLanguage, layerQuery)
            }
        } else {
            GetConfig
        }

        this.dispatcher.sendRequestToServer(request) {
            if (it.isError()) {
                val responseErrorTl = it as TLRpcError
                val responseError = TLConverter.error(responseErrorTl)
                error.invoke(responseError)
            } else {
                assert(it is TLConfig)
                val responseTl = it as TLConfig
                val response = TLConverter.config(responseTl)
                applyDcList(response)
                result.invoke(response)
            }
        }
    }

    private fun applyDcList(config: Config) {
        if (config.dcList.isEmpty()) {
            return
        }

        val dcs = config.dcList
            .distinct()
            .sortedByDescending { d -> d.priority }
            .map { d -> DcAddress(d.id, d.address, d.port, d.media) }
            .toSet()

        ServersManager.setServerAddresses(dcs)
    }
}
