package ru.kiporskiy.im.messaging

import ru.kiporskiy.im.messaging.dto.TLObject

/**
 * Класс для планирования отправки сообщений на сервер
 */
interface MessageDispatcher {

    /**
     * Отправить запрос на сервер и обработать ответ от него
     *
     * @param obj содержимое запроса, отправляемого на сервер
     * @param responseHandler (опционально) действие, выполняемое при получении ответа
     */
    fun sendRequestToServer(obj: TLObject, responseHandler: ((obj: TLObject) -> Unit)? = null)

}
