package ru.kiporskiy.im.messaging

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.kiporskiy.im.domain.ServerConnectionStatus
import ru.kiporskiy.im.event.EventManager
import ru.kiporskiy.im.help.MAX_SENT_MESSAGES_PORTION_SIZE
import ru.kiporskiy.im.messaging.dto.TLObject
import ru.kiporskiy.im.messaging.dto.TLObjectUtils
import ru.kiporskiy.im.messaging.router.ResponseMessageRouter
import ru.kiporskiy.im.transport.Sender
import ru.kiporskiy.im.transport.event.ConnectionEvent
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Класс для планирования отправки сообщений на сервер. "Планирование", потому что сообщение отправляется не всегда
 * моментально, существует небольшая задержка. В случае, если клиент не подключен к серверу, то сообщения копятся в
 * буффере, а после подключения отправляются порциями.
 *
 * Основная работа класса:
 * 1. получить сообщение для отправки на сервер
 * 2. добавить сообщение в очередь
 * 3. запустить счетчик для отправки сообщений
 * 4. по истечении счетчика получить из буфера порцию накопленных сообщений
 * 5. передать сообщения на отправку
 */
class MessageDispatcherImpl(
    private val sender: Sender,
    private val tlUtils: TLObjectUtils,
    private val msgManager: MessageManager,
    private val notificationRouter: ResponseMessageRouter,
    private val connectionState: ServerConnectionStatus
) : MessageDispatcher {

    /**
     * если true, то выполняется небольшая задержка перед отправкой сообщений серверу
     */
    private var waiting = AtomicBoolean(false)

    init {
        //подписаться на событие о подключении к серверу
        EventManager.subscribe(ConnectionEvent.EVENT_TYPE) { event ->
            //для данного типа события достпустимы только события, имеющие класс ConnectionEvent
            require(event is ConnectionEvent)
            //если подключение выполнено - вызвать функцию, отрабатывающую подключение к серверу
            if (event.connected) this.onConnect()
        }
    }

    override fun sendRequestToServer(obj: TLObject, responseHandler: ((obj: TLObject) -> Unit)?) {
        val message = Message.of(obj)

        this.msgManager.addMessage(message)

        if (responseHandler != null)
            this.notificationRouter.setResponseHandler(message.id, responseHandler)

        //проверить, что подключение с сервером установлено и на текущий момент не выполняется ожидание отправки
        if (connectionState.isConnected() && waiting.compareAndSet(false, true)) {
            //небольшая пауза перед отправкой сообщения
            GlobalScope.launch {
                //если ожидания в текущий момент нет - то пометить,
                //что осуществляется ожидание и запустить задержку отправки
                try {
                    delay(10)
                    this@MessageDispatcherImpl.post()
                } finally {
                    waiting.set(false)
                }
            }
        }
    }

    private fun onConnect() {
        this.post()
    }

    /**
     * Выполнить отправку накопленных сообщений на сервер
     *
     * @param cnt - служебный параметр для исключения слишком глубокой рекурсии
     */
    @Synchronized
    private fun post(cnt: Int = 0) {
        if (cnt < 100 && this.connectionState.isConnected() && this.msgManager.getMessagesCount() > 0) {
            //получить сообщения
            val messages = this.msgManager.getMessages()
            //десериализовать данные
            val bytes = this.tlUtils.serialize(messages)
            //отправить данные на сервер
            this.sender.send(bytes) { success -> this.msgManager.sentMessages(messages, success) }
            //если количество сообщений совпадает с максимальным размером порции для отправки сообщений, то рекурсивно
            //вызвать повторную отправку
            if (messages.size == MAX_SENT_MESSAGES_PORTION_SIZE)
                this.post(cnt + 1)
        }
    }
}
