package ru.kiporskiy.im.messaging

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import ru.kiporskiy.im.help.MAX_SENT_MESSAGES_PORTION_SIZE
import ru.kiporskiy.im.help.REPEAT_MESSAGES_SEND_DURATION
import ru.kiporskiy.im.help.REPEAT_TASK_DURATION
import ru.kiporskiy.im.messaging.dto.tl.TLMsgsAck
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.LinkedHashMap
import kotlin.concurrent.withLock

/**
 * Менеджер сообщений.
 * Хранит список сообщений, которые необходимо отправить на сервер. Сообщения отправляются на сервер порциями по
 * 20 штук максимум.
 * Имеет 3 коллекции:
 * 1. Очередь сообщений на обработку. Из этой очереди берутся сообщения для будущей отправки серверу. Максимальное количество
 * хранимых сообщений - 1тыс. Если количество сообщений превысит это число, то 10% старых сообщений будут удалены из очереди.
 * 2. Коллекция сообщений, которые были запрошены для отправки, но еще неизвестно, была ли попытка отправки удачной.
 * Размер коллекции также не превышает 1000 штук.
 * 3. Коллекция сообщений, которые были отправлены серверу, но сервер пока не прислал ни подтверждение о получении,
 * ни ответ на запрос, содержащийся в сообщении. Сообщения, не требующие подтверждения в эту коллекцию не будут попадать.
 * <p>
 * Все действия выполняются в синхронном режиме.
 * Если на сообщение в 3 коллекции от сервера долго нет подтверждения о получении, то через 15 сек. сообщение переходит
 * в первую коллекцию для повторной отправки серверу.
 */
class MessageManager {

    companion object {
        private val log = LoggerFactory.getLogger(MessageManager::class.java)

        /**
         * Максимсальное количество сообщений в очереди
         */
        const val MAX_MESSAGES_QUEUE = 1000
    }

    /**
     * Очередь сообщений на отправку
     */
    private val messagesQueue = LinkedList<MessageState>()

    /**
     * Сообщения "ушли" на отправку, но пока неизвестно были ли они отправлены
     */
    private val stashedMessages = LinkedHashMap<Long, MessageState>()

    /**
     * Очередь неподтвержденных сервером сообщений
     */
    private val sentMessages = LinkedHashMap<Long, MessageState>()

    /**
     * Блокировщик для исключения параллельного доступа
     */
    private val lock: Lock = ReentrantLock(true)

    /**
     * Список идентификаторов сообщений, который нужно отправить на сервер для подтверждения о получении от него сообщений
     */
    private var acks = LinkedList<Long>()

    init {
        //задача на попытки повторной отправки сообщений
        GlobalScope.launch {
            while (true) {
                delay(REPEAT_TASK_DURATION.toMillis())
                resend()
            }
        }
    }

    /**
     * Добавить сообщение в очередь на отправку
     */
    fun addMessage(message: Message) {
        if (log.isDebugEnabled) {
            log.debug("Добавлено новое сообщение: {}", message)
        }

        lock.withLock {
            //добавить сообщение в очередь
            this.messagesQueue.add(MessageState(message))
            //если сообщений слишком много - почистить список
            if (this.messagesQueue.size >= MAX_MESSAGES_QUEUE) {
                clearMessagesQueue()
            }
        }
    }

    private fun clearMessagesQueue() {
        lock.withLock {
            //количество сообщений, которое должно остаться
            val messagesCount = (MAX_MESSAGES_QUEUE * 0.9).toInt()

            //количетсво сообщений, которое нужно удалить
            var removeCount = this.messagesQueue.size - messagesCount

            if (log.isDebugEnabled) {
                log.debug("Remove {} elements from queue", removeCount)
            }

            //удаление элементов
            while (removeCount-- > 0) this.messagesQueue.removeFirst()
        }
    }

    /**
     * Сервер подтвердил получение сообщений
     */
    fun ack(messagesIds: Set<Long>) {
        if (log.isDebugEnabled) {
            log.debug("Ack messages: {}", messagesIds)
        }
        lock.withLock {
            messagesIds.forEach {
                this.sentMessages.remove(it)
                this.stashedMessages.remove(it)
            }
            this.messagesQueue.removeIf { messagesIds.contains(it.msg.id) }
        }
    }

    /**
     * Получить порцию сообщений для отправки клиенту
     */
    fun getMessages(): List<Message> {
        lock.withLock {
            val result = LinkedList<Message>()
            val iterator = this.messagesQueue.iterator()

            if (!this.acks.isEmpty()) {
                val ackIds = this.acks
                this.acks = LinkedList()
                result.add(Message.of(TLMsgsAck.of(ackIds)))
            }

            if (this.stashedMessages.size > MAX_MESSAGES_QUEUE) {
                log.warn("Too many stashed messages: {}", this.stashedMessages.size)
                this.stashedMessages.clear()
            }

            while (iterator.hasNext() && result.size < MAX_SENT_MESSAGES_PORTION_SIZE) {
                val messageState = iterator.next()
                iterator.remove()
                result.add(messageState.msg)
                this.stashedMessages[messageState.msg.id] = messageState
            }

            return result
        }
    }

    /**
     * Сообщения отправлены на сервер (но от сервера не получено подтверждение)
     */
    fun sentMessages(messages: List<Message>, success: Boolean = true) {
        lock.withLock {
            val stashed = messages.mapNotNull { stashedMessages.remove(it.id) }

            if (success) {
                stashed.filter { it.msg.ack }
                        .forEach {
                            it.lastSentTime = LocalDateTime.now()
                            this.sentMessages[it.msg.id] = it
                        }

                //если сообщений слишком много - почистить список
                if (this.sentMessages.size > MAX_MESSAGES_QUEUE) {
                    clearSentMessagesQueue()
                }
            } else {
                stashed.asReversed().forEach { this.messagesQueue.addFirst(it) }
            }
        }
    }

    private fun clearSentMessagesQueue() {
        //количество сообщений, которое должно остаться
        val messagesCount = (MAX_MESSAGES_QUEUE * 0.9).toInt()

        //количетсво сообщений, которое нужно удалить
        val removeCount = this.sentMessages.size - messagesCount

        if (log.isDebugEnabled) {
            log.debug("Remove {} elements from sent messages", removeCount)
        }

        //идентификаторы сообщений
        val ids = this.sentMessages.asSequence()
                .map { it.key }
                .take(removeCount).toList()

        //удаление сообщений
        ids.forEach { this.sentMessages.remove(it) }
    }

    /**
     * Переотправить неподтвержденные сообщения
     */
    private fun resend() {
        lock.withLock {
            val now = LocalDateTime.now()
            val repeatMessages = this.sentMessages.values
                    .filter { it.lastSentTime!!.plus(REPEAT_MESSAGES_SEND_DURATION).isBefore(now) }
            repeatMessages.forEach {
                this.sentMessages.remove(it.msg.id)
                this.messagesQueue.addFirst(it)
            }
        }
    }

    /**
     * Получить кол-во сообщений, готовых к отправке
     */
    fun getMessagesCount() = messagesQueue.size

    /**
     * Список идентификаторов для отправки подтверждения о получении на сервер
     */
    fun sendAck(ids: List<Long>) {
        ids.forEach { acks.add(it) }
    }

    /**
     * Состояние отправляемого сообщения.
     */
    data class MessageState(

            /**
             * Само сообщение
             */
            val msg: Message,

            /**
             * Время создания сообщения
             */
            val createTime: LocalDateTime = LocalDateTime.now(),

            /**
             * Последняя попытка отправки сообщения
             */
            var lastSentTime: LocalDateTime? = null)
}
