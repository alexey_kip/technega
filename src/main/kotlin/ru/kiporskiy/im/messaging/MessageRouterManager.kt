package ru.kiporskiy.im.messaging

import ru.kiporskiy.im.messaging.router.MessageRouter

/**
 * Класс, хранящий список всех маршрутизаторов уведомлений, поступающих от сервера
 */
interface MessageRouterManager {

    /**
     * Добавить новый маршрутизатор
     */
    fun addMsgHandler(router: MessageRouter)

}
