package ru.kiporskiy.im.messaging

import ru.kiporskiy.im.messaging.dto.TLObject

/**
 * Обёртка над запросами к серверу и ответами от него.
 */
interface Message {

    companion object {
        /**
         * Создать новое сообщение
         */
        fun of(obj: TLObject) = of(obj, IdGenerator.getId(), obj.ack())

        fun of(obj: TLObject, id: Long, ack: Boolean) = MessageImpl(id, obj, ack)
    }

    /**
     * Идентификатор сообщения
     */
    val id: Long

    /**
     * Содержимое сообщения
     */
    val data: TLObject

    /**
     * Требуется ли подтверждать получение сообщения
     */
    val ack: Boolean

}

data class MessageImpl(override val id: Long,
                  override val data: TLObject,
                  override val ack: Boolean = data.ack()) : Message
