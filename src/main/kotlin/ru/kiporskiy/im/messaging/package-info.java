/**
 * Классы для обмена сообщениями с сервером.
 * Пакет {@link ru.kiporskiy.im.messaging.mediator} содержит классы, являющиеся реализацией Actor'ов, которыми пользуется
 * бизнес логика для запроса данных от сервера.
 * Пакет {@link ru.kiporskiy.im.messaging.dto} содержит классы, объектами которых клиент и сервер обмениваются через
 * транспортный уровень.
 * Класс {@link ru.kiporskiy.im.messaging.IdGenerator} для генерации идентификатора очередного сообщения перед
 * отправкой его на сервер.
 * Класс {@link ru.kiporskiy.im.messaging.NotificationRouter} класс для перенаправления всех получаемых уведомлений
 * от сервера нужному обработчику.
 * Класс {@link ru.kiporskiy.im.messaging.MessageManager} хранит сообщения, которые должны в ближайшее время
 * отправиться на сервер.
 * Класс {@link ru.kiporskiy.im.messaging.MessageDispatcher} используется при необходимости что-то отправить на сервер.
 * Класс {@link ru.kiporskiy.im.messaging.Message} обертка над классами DTO, описывающая полученное/переданное
 * сообщение с его идентификатором и номером в последовательности.
 */
package ru.kiporskiy.im.messaging;
