package ru.kiporskiy.im.domain

/**
 * Класс для проверки наличия активного подключения к серверу
 */
interface ServerConnectionStatus {

    /**
     * @return true, если подключение присутствует, иначе - false
     */
    fun isConnected(): Boolean

}
