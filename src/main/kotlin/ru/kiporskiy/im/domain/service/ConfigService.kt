package ru.kiporskiy.im.domain.service

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import ru.kiporskiy.im.core.help.Config
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.Mediator
import ru.kiporskiy.im.help.TimeSettings
import ru.kiporskiy.im.help.Times.minute
import ru.kiporskiy.im.help.Times.second

/**
 * Обновление конфигураций.
 * Периодически запрашивает от сервера конфиги. В первом запросе отправляет всю информацию о текущем клиенте.
 * В случае ошибки выполняется повторный запрос.
 */
class ConfigService(private val mediator: Mediator) {

    //клиент уже отправил на сервер описание системы
    private var sentSystemInfo = false

    companion object {
        private val log = LoggerFactory.getLogger(ConfigService::class.java)

        //запрос конфигов от сервера 1 раз в 5 минут
        private val UPDATE_CONFIG_TIMEOUT = 5.minute

        //первичный запрос конфигов от сервера каждую 1 сек
        private val FIRST_CONFIG_TIMEOUT = 1.second

    }

    init {
        GlobalScope.launch {
            while (true) {
                //получить конфиги
                getConfig()

                //подождать некоторое время
                if (!this@ConfigService.sentSystemInfo) {
                    delay(FIRST_CONFIG_TIMEOUT)
                } else {
                    delay(UPDATE_CONFIG_TIMEOUT)
                }
            }
        }
    }

    /**
     * Отправить запрос конфигурации на сервер
     */
    private fun getConfig() {
        this.mediator.help.getConfig(!sentSystemInfo, this::updateConfig, this::onError)
    }

    //Вариант 1. Функция завершилась успехом
    private fun updateConfig(config: Config) {
        //после получения первого ответа от сервера отправлять данные о клиенте для сервера больше не надо
        sentSystemInfo = true

        //обновить серверное время для "себя"
        TimeSettings.updateServerTime(config.date)

        if (log.isInfoEnabled)
            log.info("Обновлено серверное время: {}", config.date)
    }

    //Вариант 2. Функция завершилась с ошибкой
    private fun onError(error: Error) {
        log.warn("Запрос <GetConfig> завершился ошибкой: {}", error)

        //после ошибки повторить отправку инфо о системе
        this.sentSystemInfo = false
    }

    /**
     * Перезапросить конфиги от сервера
     */
    fun updateConfigs() {
        this.getConfig()
    }
}
