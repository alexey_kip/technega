package ru.kiporskiy.im.domain.service

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import ru.kiporskiy.im.domain.Mediator
import ru.kiporskiy.im.domain.ServerConnectionStatus
import java.time.Instant
import java.util.concurrent.atomic.AtomicLong

/**
 * Класс для отправки команд пинга и понга на сервер.
 * Через каждые PING_DELAY мсек происходит отправка пинга на сервер.
 * Класс работает самостоятельно после создания экземпляра и не требует специального вызова
 */
class PingPongService(private val mediator: Mediator, private val serverConnectionStatus: ServerConnectionStatus) {

    companion object {
        private val log = LoggerFactory.getLogger(PingPongService::class.java)

        /**
         * Счетчик отправленных пингов на сервер
         */
        private var pingCounter = 0L

        /**
         * Задержка между пингами по умолчанию
         */
        internal const val PING_DELAY = 10_000L
    }

    //время последней задержки между пингом и понгом
    private var lastPingPongDelay = AtomicLong(0)

    //время отправки пингов
    private val pings = mutableMapOf<Long, Instant>()

    init {
        //запуск задания на бесконечную отправку пингов на сервер
        GlobalScope.launch(Dispatchers.IO) {
            while (true) {
                delay(PING_DELAY)
                sendPing()
            }
        }
    }

    /**
     * Отправка пинга на сервер
     */
    fun sendPing() {
        if (!serverConnectionStatus.isConnected()) {
            return
        }

        val pingId = nextPingId()

        this.mediator.help.runPing(pingId)

        if (this.pings.size > 100) {
            this.pings.clear()
        }

        this.pings[pingId] = Instant.now()

        if (log.isDebugEnabled) log.debug("Отправка пинг $pingId")
    }

    private fun nextPingId() = pingCounter++

    /**
     * Ответ на пинг сервера
     */
    fun sendPong(msgId: Long, pingId: Long) {
        this.mediator.help.runPong(msgId, pingId)
        if (log.isDebugEnabled) log.debug("Отправка понг на пинг $pingId")
    }

    /**
     * Реакция на понг от сервера
     */
    fun onPong(pingId: Long) {
        val time = this.pings.remove(pingId)?.toEpochMilli()
        val delay = time?.let { Instant.now().minusMillis(it).toEpochMilli() } ?: 0L
        lastPingPongDelay.set(delay)
        if (log.isDebugEnabled) {
            log.debug("Получен ответ на пинг $pingId. Задержка $delay мсек")
        }
    }

    fun getLastDelay() = lastPingPongDelay.get()
}
