package ru.kiporskiy.im.domain.service

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.core.auth.CodeType
import ru.kiporskiy.im.core.auth.SentCode
import ru.kiporskiy.im.core.help.Error
import ru.kiporskiy.im.domain.Mediator
import java.lang.IllegalStateException
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Сервис для создания авторизаций
 */
class AuthService(private val mediator: Mediator) {

    companion object {
        private val log = LoggerFactory.getLogger(AuthService::class.java)
    }

    /**
     * Параметры кода, отправленного в прошлый раз
     */
    private var previousCode: SentCode? = null

    /**
     * Номер телефона, на который был отправлен код в прошлый раз
     */
    private var previousPhoneNumber: String? = null

    /**
     * Контроллирует, чтобы код не был отправлен дважды
     */
    private val sentCodeWithoutResponse: AtomicBoolean = AtomicBoolean(false)


    /**
     * Отправка кода для логина в приложение
     */
    @Synchronized
    fun sendCode(
        phoneNumber: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    ) {
        //либо код отправляется первый раз, либо номер телефона должен совпадать при переотправке
        require(previousPhoneNumber == null && previousCode == null || previousPhoneNumber == phoneNumber)

        //была ли выполнена попытка запросить код?
        if (this.sentCodeWithoutResponse.compareAndExchangeRelease(false, true))
        //если да, то ничего не делать
            return

        //известен ли следующий тип отправки кода?
        val codeType = getNextSendCodeType()
        if (codeType == null) {
            //исключение, т.к. UI должен был убедиться, что отправлять код в приложение можно
            throw IllegalStateException("Не удалось вычислить следующий тип отправки кода для авторизации")
        }

        //отправить код на указанный номер телефона
        if (this.previousCode == null) {
            //если код еще не был отправлен ниразу
            log.info("Команда: Отправить код на номер телефона ${phoneNumber}")
            this.sendCodeFirst(phoneNumber, result, error)
            this.previousPhoneNumber = phoneNumber
        } else {
            //если код уже был отправлен
            log.info("Команда: Повторно отправить код на номер телефона ${phoneNumber}")
            this.resendCode(result, error)
        }
    }

    /**
     * Получить следующий тип отправки сообщения
     */
    private fun getNextSendCodeType(): CodeType? {
        return this.previousCode?.nextCodeType ?: CodeType.APP
    }

    //отправить код первый раз
    private fun sendCodeFirst(
        phoneNumber: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    ) {
        this.mediator.auth.sendCode(phoneNumber, { okResult ->
            this.sentCodeWithoutResponse.set(false)
            this.previousCode = okResult
            result.invoke(okResult)
        }, { failResult ->
            this.sentCodeWithoutResponse.set(false)
            this.previousCode = null
            this.previousPhoneNumber = null
            error.invoke(failResult)
        })
    }

    //переотправить код повторно
    private fun resendCode(
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    ) {
        val phone = this.previousPhoneNumber!!
        val codeHash = this.previousCode!!.codeHash
        this.mediator.auth.resendCode(phone, codeHash, { okResult ->
            this.sentCodeWithoutResponse.set(false)
            this.previousCode = okResult
            result.invoke(okResult)
        }, { failResult ->
            this.sentCodeWithoutResponse.set(false)
            error.invoke(failResult)
        })
    }

    /**
     * Сбросить отправленный код
     */
    fun cancelCode(
        result: (sentCodeResult: Boolean) -> Unit,
        error: (error: Error) -> Unit
    ) {
        //код уже должен быть отправлен
        require(previousPhoneNumber != null && previousCode != null)

        val phone = this.previousPhoneNumber!!
        val codeHash = this.previousCode!!.codeHash

        log.info("Команда: Отменить отправку кода на номер ${phone}")

        //отменить код
        this.mediator.auth.cancelCode(phone, codeHash, result, error)

        //сбросить состояние объекта
        this.previousCode = null
        this.previousPhoneNumber = null
        this.sentCodeWithoutResponse.set(false)
    }
}
