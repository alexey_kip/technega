package ru.kiporskiy.im.domain.service

/**
 * Объект содержит список всех доступных сервисов
 */
object MasterService {

    lateinit var authService: AuthService
        private set

    lateinit var configService: ConfigService
        private set

    lateinit var pingPongService: PingPongService
        private set

    fun initServices(authService: AuthService, configService: ConfigService, pingPongService: PingPongService) {
        this.authService = authService
        this.configService = configService
        this.pingPongService = pingPongService
    }

}
