package ru.kiporskiy.im.domain

import ru.kiporskiy.im.core.auth.SentCode
import ru.kiporskiy.im.core.help.Config
import ru.kiporskiy.im.core.help.Error

/**
 * Посредник между бизнес логикой и транспортным уровнем. Генерирует TL запрос, отправляет кго в очередь
 * для последующей отправки на сервер.
 *
 * @param auth посрединк для отправки запросов авторизации
 * @param help посрединк для отправки вспомогательных запросов
 */
class Mediator(
    val help: HelpMediator,
    val auth: AuthMediator
)


/**
 * Вспомогательные запросы (получение конфигов, идентификатора ближайшего сервера и др.)
 */
interface HelpMediator {

    /**
     * Отправить команду ping на сервер.
     * Проверяет доступность сервера, сообщает серверу, что клиент "живой" и поддерживает с ним связь.
     *
     * @param id идентификатор пинга. В Pong в качестве свойства id будет указан этот идентификатор.
     */
    fun runPing(id: Long)

    /**
     * Отправить команду pong на сервер. Является ответом на ping.
     *
     * @param messageId идентификатор сообщения с пингом от сервера
     * @param id идентификатор пинга от сервера
     */
    fun runPong(messageId: Long, id: Long)

    /**
     * Получить конфиги приложения
     *
     * @param serviceInfoRequired клиент отправит на сервер информацию о системе, на которой сейчас работает приложение
     * @param result действие, которое нужно совершить с полученными конфигами
     * @param error действие, которое нужно совершить в случае ошибки
     */
    fun getConfig(serviceInfoRequired: Boolean, result: (configResult: Config) -> Unit, error: (error: Error) -> Unit)

}

/**
 * Запросы, связанные с авторизацией
 */
interface AuthMediator {

    /**
     * Отправить код для входа на другое устройство, либо в смс
     *
     * @param phoneNumber номер телефона, на который отправлять код
     * @param result действие, которое нужно совершить после ответа сервера об отправленном коде
     * @param error действие, которое нужно совершить в случае ошибки
     */
    fun sendCode(
        phoneNumber: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    )

    /**
     * Повторно отправить код для входа, используя другой способ (СМС или звонок)
     *
     * @param phoneNumber номер телефона, на который отправлять код
     * @param phoneCodeHash хэш кода, отправленного в предыдущий раз
     * @param result действие, которое нужно совершить после ответа сервера об отправленном коде
     * @param error действие, которое нужно совершить в случае ошибки
     */
    fun resendCode(
        phoneNumber: String,
        phoneCodeHash: String,
        result: (sentCodeResult: SentCode) -> Unit,
        error: (error: Error) -> Unit
    )

    /**
     * Сбросить отправленный код
     *
     * @param phoneNumber номер телефона, на который отправлен код
     * @param phoneCodeHash хэш отправленного кода
     * @param result действие, которое нужно совершить после ответа сервера о сбросе кода
     * @param error действие, которое нужно совершить в случае ошибки
     */
    fun cancelCode(
        phoneNumber: String,
        phoneCodeHash: String,
        result: (codeCancelled: Boolean) -> Unit,
        error: (error: Error) -> Unit
    )

}
