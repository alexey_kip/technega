package ru.kiporskiy.im.core.user

object EmptyUserParams : UserParams {

    override fun <T> get(arg: Argument<T>) = null

    override fun <T> getOrElse(arg: Argument<T>, alt: T) = alt

    override fun <T> set(key: Argument<T>, value: T) {
        throw UnsupportedOperationException()
    }
}
