package ru.kiporskiy.im.core.user

import ru.kiporskiy.im.core.peer.PeerType
import ru.kiporskiy.im.core.peer.settings.NotifySettings
import java.time.LocalDateTime

open class DefaultUser(override val id: UserId,
                       override val firstName: String,
                       override val lastName: String,
                       override val phoneNumber: String,
                       override val username: String,
                       override val self: Boolean,
                       override val params: UserParams,
                       override val notifySettings: NotifySettings,
                       override val onlineDate: LocalDateTime? = null) : User {

    override val type = PeerType.USER

}
