package ru.kiporskiy.im.core.user

import org.slf4j.LoggerFactory

class WorkerParams : UserParams {

    companion object {
        private val log = LoggerFactory.getLogger(WorkerParams::class.java)

        val OFFICE_NUMBER = Argument<String>("OFFICE_NUMBER")
    }

    private val params = HashMap<Any, Any>()

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(arg: Argument<T>) = this.params.get(arg) as T?

    override fun <T> getOrElse(arg: Argument<T>, alt: T) = get(arg) ?: alt

    override fun <T> set(key: Argument<T>, value: T) {
        params.put(key, value!!)
    }
}
