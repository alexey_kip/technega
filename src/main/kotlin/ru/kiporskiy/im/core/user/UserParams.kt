package ru.kiporskiy.im.core.user

/**
 * Параметры пользователя
 */
interface UserParams {

    /**
     * Получить значение
     */
    fun <T> get(arg: Argument<T>): T?

    /**
     * Получить значение, если не установлено - вренуть значение по умолчанию
     */
    fun <T> getOrElse(arg: Argument<T>, alt: T): T

    /**
     * Задать значение
     */
    fun <T> set(key: Argument<T>, value: T)

}

data class Argument<out Object>(val name: String)
