package ru.kiporskiy.im.core.user

import ru.kiporskiy.im.core.peer.Peer
import java.time.LocalDateTime

/**
 * Пользоватлель
 */
interface User : Peer {

    /**
     * Имя
     */
    val firstName: String

    /**
     * Фамилия
     */
    val lastName: String

    /**
     * Юзер - текущий пользователь
     */
    val self: Boolean

    /**
     * Получить дополнительные параметры пользователя
     */
    val params: UserParams

    /**
     * Номер телефона
     */
    val phoneNumber: String

    /**
     * Имя пользователя
     */
    val username: String

    /**
     * Дата, когда пользователь последний раз был в сети
     */
    val onlineDate: LocalDateTime?

}
