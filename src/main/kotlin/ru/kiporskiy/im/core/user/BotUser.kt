package ru.kiporskiy.im.core.user

import ru.kiporskiy.im.core.peer.PeerType
import ru.kiporskiy.im.core.peer.settings.NotifySettings

open class BotUser(override val id: UserId,
                   override val firstName: String,
                   override val username: String,
                   override val notifySettings: NotifySettings) : User {

    override val type = PeerType.BOT

    override val lastName = ""

    override val self = false

    override val params = EmptyUserParams

    override val phoneNumber = ""

    override val onlineDate = null
}
