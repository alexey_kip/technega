package ru.kiporskiy.im.core.user

import ru.kiporskiy.im.core.peer.PeerId

/**
 * Уникальный идентификатор пользователя
 */
data class UserId(override val id: Int, val accessHash: Long) : PeerId
