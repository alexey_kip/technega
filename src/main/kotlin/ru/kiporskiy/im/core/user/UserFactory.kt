package ru.kiporskiy.im.core.user

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.core.peer.settings.NotifySettings
import ru.kiporskiy.im.help.TimeUtils
import java.time.Instant
import java.time.LocalDateTime
import java.util.regex.Pattern

/**
 * Класс для построения пользователей
 */
class UserFactory {

    companion object {
        internal val MAX_FIRSTNAME_CHARS = 50
        internal val MAX_LASTNAME_CHARS = 50
        internal val USERNAME_PATTERN = Pattern.compile("^[0-9a-zA-Z_-]{3,30}$")
        internal val PHONE_NUMBER = Pattern.compile("^[0-9]{5,20}$")

        fun getUserBuilder() = DefaultUserBuilder()
    }

    class DefaultUserBuilder {
        companion object {
            private val log = LoggerFactory.getLogger(DefaultUserBuilder::class.java)
        }

        private lateinit var userId: UserId

        private lateinit var firstName: String

        private lateinit var lastName: String

        private lateinit var username: String

        private lateinit var phoneNumber: String

        private var onlineDate: LocalDateTime? = null

        private var isSelf: Boolean = false

        private val userParams = WorkerParams()

        private lateinit var notifySettings: NotifySettings

        fun setId(id: Int, accessHash: Long) {
            assert(id > 0) { "Id lt 0: $id" }
            this.userId = UserId(id, accessHash)
        }

        fun setFirstName(firstName: String) {
            if (firstName.length > MAX_FIRSTNAME_CHARS) {
                log.warn("Too long first name $firstName")
            }
            this.firstName = firstName.slice(IntRange(0, MAX_FIRSTNAME_CHARS - 1))
        }

        fun setLastName(lastName: String) {
            if (lastName.length > MAX_LASTNAME_CHARS) {
                log.warn("Too long last name $lastName")
            }
            this.lastName = lastName.slice(IntRange(0, MAX_LASTNAME_CHARS - 1))
        }

        fun setUsername(username: String) {
            if (!USERNAME_PATTERN.matcher(username).find()) {
                log.warn("Incorrect username $username")
            }
            this.username = username
        }

        fun isSelf() {
            this.isSelf = true
        }

        fun setPhoneNumber(phoneNumber: String) {
            if (!PHONE_NUMBER.matcher(phoneNumber).find()) {
                log.warn("Incorrect username $phoneNumber")
            }
            this.phoneNumber = phoneNumber
        }

        fun setOnlineDate(time: Int) {
            if (time <= 0) {
                this.onlineDate = null
            } else if (time > Instant.now().epochSecond) {
                this.onlineDate = LocalDateTime.now()
            } else {
                this.onlineDate = TimeUtils.epochSecondToLocalDateTime(time)
            }
        }

        fun <T> setParam(key: Argument<T>, value: T) {
            this.userParams.set(key, value)
        }

        fun setNotifySettings(settings: NotifySettings) {
            this.notifySettings = settings
        }

        fun build() = DefaultUser(this.userId,
                this.firstName,
                this.lastName,
                this.phoneNumber,
                this.username,
                this.isSelf,
                this.userParams,
                this.notifySettings,
                this.onlineDate)
    }
}
