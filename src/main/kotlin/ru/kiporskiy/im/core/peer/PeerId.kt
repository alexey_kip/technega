package ru.kiporskiy.im.core.peer

/**
 * Идентикиатор пира
 */
interface PeerId {
    val id: Int
}
