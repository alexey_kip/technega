package ru.kiporskiy.im.core.peer

import ru.kiporskiy.im.core.channel.Channel
import ru.kiporskiy.im.core.chat.Chat
import ru.kiporskiy.im.core.user.User

class PeerStorage {

    /**
     * Типы пиров
     */
    private val peersTypes = HashMap<PeerId, PeerType>()

    /**
     * Пользователи
     */
    private val users = HashMap<PeerId, User>()

    /**
     * Чаты
     */
    private val chats = HashMap<PeerId, Chat>()

    /**
     * Каналы
     */
    private val channels = HashMap<PeerId, Channel>()


    /**
     * Получить пользователя по идентификатору
     */
    fun getUserOrBot(id: PeerId): User? = users[id]

    /**
     * Получить чат по идентификатору
     */
    fun getChat(id: PeerId): Chat? = chats[id]

    /**
     * Получить канал по идентификатору
     */
    fun getChannelOrMegagroup(id: PeerId): Channel? = channels[id]

    /**
     * Добавить пира в хранилище
     */
    fun put(peer: Peer) {
        when (peer) {
            is Channel -> channels[peer.id] = peer
            is Chat -> chats[peer.id] = peer
            is User -> users[peer.id] = peer
            else -> throw UnsupportedOperationException(peer.toString())
        }
    }
}
