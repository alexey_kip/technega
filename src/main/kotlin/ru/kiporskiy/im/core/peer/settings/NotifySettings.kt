package ru.kiporskiy.im.core.peer.settings

/**
 * Настройки уведомлений
 */
data class NotifySettings(
        /**
         * Уведомления выключены
         */
        val mute: Boolean
) {

    companion object {
        val DEFAULT = NotifySettings(false)
    }

}
