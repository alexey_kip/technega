package ru.kiporskiy.im.core.peer

/**
 * Тип пира
 */
enum class PeerType {
    /**
     * Пользователь
     */
    USER,
    /**
     * Бот
     */
    BOT,
    /**
     * Группа
     */
    CHAT,
    /**
     * Канал
     */
    CHANNEL,
    /**
     * Супергруппа
     */
    MEGAGROUP,
    /**
     * Рассылка
     */
    BROADCASTING
}
