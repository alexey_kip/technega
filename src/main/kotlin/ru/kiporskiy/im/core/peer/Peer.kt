package ru.kiporskiy.im.core.peer

import ru.kiporskiy.im.core.peer.settings.NotifySettings

/**
 * Пир (пользователь/бот/чат/канал)
 */
interface Peer {

    /**
     * Идентификатор пира
     */
    val id: PeerId

    /**
     * Тип пира
     */
    val type: PeerType

    /**
     * Настройки уведомлений
     */
    val notifySettings: NotifySettings
}
