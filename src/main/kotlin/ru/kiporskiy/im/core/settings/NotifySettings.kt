package ru.kiporskiy.im.core.settings

import java.time.Instant

/**
 * Настройки уведомлений
 *
 * @param mute уведомления выключены
 * @param muteUntil уведомления выключены до указанной даты
 */
data class NotifySettings(val mute: Boolean, val muteUntil: Instant) {

    /**
     * Проверить, что текущие настройки уже недействительны
     */
    fun isExpired() = !mute && muteUntil.isBefore(Instant.now())

}
