package ru.kiporskiy.im.core.settings

import ru.kiporskiy.im.core.peer.Peer
import ru.kiporskiy.im.core.peer.PeerType
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

/**
 * Объект хранит все настройки уведомлений.
 *
 * Существуют 3 вида общих настроек: для пользователей, для чатов и для каналов, также, для каждого участника могут
 * настраиваться исключения. Например, настройки "для пользователей выключены уведомления", но для Иванова Ивана
 * уведомления могут быть включены (это и есть исключение из общего правила).
 */
object PeerNotifySettings {

    /**
     * Настройки уведомлений по умолчанию
     */
    private val defaultSettings = NotifySettings(false, Instant.now())

    /**
     * Настройки уведомлений с пользователями
     */
    private var users: NotifySettings = defaultSettings.copy()

    /**
     * Настройки уведомлений с чатами
     */
    private var chats: NotifySettings = defaultSettings.copy()

    /**
     * Настройки уведомлений с каналами
     */
    private var channels: NotifySettings = defaultSettings.copy()

    /**
     * Исключения из правил настроек уведомлений с пользователями
     */
    private val usersExceptions: MutableMap<Int, NotifySettings> = ConcurrentHashMap()

    /**
     * Исключения из правил настроек уведомлений с чатами
     */
    private val chatsExceptions: MutableMap<Int, NotifySettings> = ConcurrentHashMap()

    /**
     * Исключения из правил настроек уведомлений с каналами
     */
    private val channelsExceptions: MutableMap<Int, NotifySettings> = ConcurrentHashMap()

    /**
     * Сбросить натсройки уведомлений на начальные значения
     */
    fun reset() {
        users = defaultSettings
        chats = defaultSettings
        channels = defaultSettings
        usersExceptions.clear()
        chatsExceptions.clear()
        channelsExceptions.clear()
    }

    /**
     * Получить настройки уведомлений для пира
     */
    fun getSettings(peer: Peer): NotifySettings {
        val personal: NotifySettings?
        val common: NotifySettings

        when (peer.type) {
            PeerType.CHAT -> {
                personal = chatsExceptions[peer.id.id]
                common = chats
            }
            PeerType.USER, PeerType.BOT -> {
                personal = usersExceptions[peer.id.id]
                common = users
            }
            else -> {
                personal = channelsExceptions[peer.id.id]
                common = channels
            }
        }

        return if (personal == null || personal.isExpired()) {
            if (common.isExpired())
                defaultSettings
            else
                common
        } else {
            personal
        }
    }

    /**
     * Получить общие настройки для пользователей
     */
    fun getUserSettings(): NotifySettings {
        return if (!users.isExpired()) users else defaultSettings
    }

    /**
     * Получить общие настройки для пользователей
     */
    fun getDefaultSettings() = defaultSettings

    /**
     * Получить общие настройки для чатов
     */
    fun getChatSettings(): NotifySettings {
        return if (!chats.isExpired()) chats else defaultSettings
    }

    /**
     * Получить общие настройки для каналов
     */
    fun getChannelSettings(): NotifySettings {
        return if (!channels.isExpired()) channels else defaultSettings
    }

    /**
     * Обновить настройки уведомлений для пира
     *
     * @return true - настройки обновлены, false - нет
     */
    fun updateSettings(peer: Peer, settings: NotifySettings): Boolean {
        //поиск текущих настроек пира
        val peerSettings = getSettings(peer)

        if (peerSettings == settings) {
            //если текущие совпадают с обновляемыми, то ничего делать не надо
            return false
        }

        //поиск подходящей коллекции в зависимости от типа пира
        val map = when (peer.type) {
            PeerType.CHAT -> chatsExceptions
            PeerType.USER, PeerType.BOT -> usersExceptions
            else -> channelsExceptions
        }

        //удаление старых настроек, если они были
        map.remove(peer.id.id)

        if (!settings.isExpired()) {
            //если новые настройки актуальны, то применить их
            map[peer.id.id] = settings.copy()
            return true
        }

        return false
    }

    /**
     * Обновить настройки уведомлений для всех пользователей
     */
    fun updateUsersSettings(settings: NotifySettings) {
        if (settings != users)
            users = if (settings.isExpired()) defaultSettings else settings

    }

    /**
     * Обновить настройки уведомлений для всех чатов
     */
    fun updateChatsSettings(settings: NotifySettings) {
        if (settings != chats)
            chats = if (settings.isExpired()) defaultSettings else settings
    }

    /**
     * Обновить настройки уведомлений для всех каналов
     */
    fun updateChannelsSettings(settings: NotifySettings) {
        if (settings != channels)
            channels = if (settings.isExpired()) defaultSettings else settings
    }
}
