package ru.kiporskiy.im.core.channel

import ru.kiporskiy.im.core.user.UserId

/**
 * Участник чата
 */
data class ChannelParticipant(val channelId: ChannelId,
                              val userId: UserId,
                              val invitedBy: UserId,
                              val kickedBy: UserId? = null,
                              val left: Boolean = false,
                              val limits: ChannelParticipantLimits,
                              val rights: ChannelParticipantRights)
