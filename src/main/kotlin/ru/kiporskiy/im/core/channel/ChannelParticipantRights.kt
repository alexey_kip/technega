package ru.kiporskiy.im.core.channel

/**
 * Права участника канала
 *
 * @param isCreator владелец канала
 * @param isAdmin администратор
 * @param canWriteMessages может писать сообщения в канал
 * @param canEditMessages может редактировать чужие сообщения
 * @param canAddParticipants может добавлять подписчиков в канал
 * @param canAddAdmins может назначать администраторов канала
 */
data class ChannelParticipantRights(val isCreator: Boolean,
                                    val isAdmin: Boolean,
                                    val canWriteMessages: Boolean,
                                    val canEditMessages: Boolean,
                                    val canAddParticipants: Boolean,
                                    val canAddAdmins: Boolean)
