package ru.kiporskiy.im.core.channel

import ru.kiporskiy.im.core.peer.Peer
import ru.kiporskiy.im.core.user.UserId
import java.net.URL

interface Channel : Peer {

    /**
     * Создатель канала
     */
    val creator: UserId

    /**
     * Участники
     */
    val participants: Map<UserId, ChannelParticipant>

    /**
     * URL чата
     */
    val channelLink: URL?

    /**
     * Название чата
     */
    val name: String

    /**
     * Описание чата
     */
    val description: String

    /**
     * Username публичного канала
     */
    val username: String?

    /**
     * Публичный канал
     */
    fun isPublic() = username != null
}
