package ru.kiporskiy.im.core.channel

/**
 * Ограничения участника канала
 *
 * @param readMessage запрещено читать сообщения
 */
data class ChannelParticipantLimits(val readMessage: Boolean)
