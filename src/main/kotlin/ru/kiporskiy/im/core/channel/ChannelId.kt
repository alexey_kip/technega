package ru.kiporskiy.im.core.channel

import ru.kiporskiy.im.core.peer.PeerId

/**
 * Идентификатор канала
 */
data class ChannelId(override val id: Int, val accessHash: Long) : PeerId
