package ru.kiporskiy.im.core.channel

import ru.kiporskiy.im.core.peer.PeerType
import ru.kiporskiy.im.core.peer.settings.NotifySettings
import ru.kiporskiy.im.core.user.UserId
import java.net.URL

data class DefaultChannel(override val id: ChannelId,
                          override val creator: UserId,
                          override val name: String,
                          override val description: String,
                          override val participants: Map<UserId, ChannelParticipant>,
                          override val channelLink: URL?,
                          override val username: String?,
                          override val notifySettings: NotifySettings) : Channel {

    override val type = PeerType.CHANNEL

}
