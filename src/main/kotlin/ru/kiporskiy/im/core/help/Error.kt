package ru.kiporskiy.im.core.help

/**
 * Описание ошибки от сервера
 */
data class Error(val code: Int, val description: String) {

    override fun toString(): String {
        return "code: $code, description: [$description]"
    }

}