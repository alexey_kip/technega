package ru.kiporskiy.im.core.help

/**
 * Информация о клиенте
 */
data class ClientInfo(val appId: Int,
                      val deviceModel: String,
                      val systemVersion: String,
                      val appVersion: String,
                      val systemLangCode: String,
                      val langPack: String,
                      val currentLanguage: String)
