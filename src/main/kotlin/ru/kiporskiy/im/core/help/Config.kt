package ru.kiporskiy.im.core.help

import java.time.Instant

/**
 * Конфиг, получаемый от сервера
 *
 * @param date      серверное время
 * @param dcList    список адресов серверов
 */
data class Config(val date: Instant, val dcList: List<Dc>)

/**
 * Адрес датацентра
 *
 * @param id        идентификатор датацентра
 * @param address   адрес сервера
 * @param port      порт
 * @param media     датацентр только для отправки/получения файлов
 * @param priority  приоритет датацентра
 */
data class Dc(val id: Int, val address: String, val port: Int, val media: Boolean, val priority: Byte = 0)
