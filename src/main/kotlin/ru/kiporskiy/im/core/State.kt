package ru.kiporskiy.im.core

import ru.kiporskiy.im.core.peer.PeerId
import java.time.Instant
import java.time.LocalDateTime

/**
 * Состояние клиента
 */
class State {

    /**
     * Значение PTS
     * Если -1, то пользователь не авторизован
     */
    var pts: Int = 0

    /**
     * Идентификатор пользователя
     * Если null, то пользователь не авторизован
     */
    var me: PeerId? = null

    /**
     * Время запуска
     */
    val startTimestamp = LocalDateTime.now()!!

    /**
     * Разница в секундах со временем на сервере
     */
    var serverTimeDiff: Int = 0
        private set


    /**
     * Преобразовать время сервера во время клиента
     */
    fun getMyTime(serverTime: Int): LocalDateTime {
        return LocalDateTime
                .from(Instant.ofEpochSecond(serverTime.toLong()))
                .plusSeconds(serverTimeDiff.toLong())
    }

    /**
     * Задать разницу во времени между клиентом и сервером
     */
    fun setServerTimeDiff(serverTime: Int) {
        val now = Instant.now().epochSecond.toInt()
        this.serverTimeDiff = serverTime - now
    }
}

