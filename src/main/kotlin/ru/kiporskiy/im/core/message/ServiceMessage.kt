package ru.kiporskiy.im.core.message

/**
 * Сервисное сообщение (пользователь добавлен в чат, пользователь исключен из чата и т.п.)
 */
data class ServiceMessage(private val id: Message.ID, private val text: String) : Message {
    override fun getId() = id
}
