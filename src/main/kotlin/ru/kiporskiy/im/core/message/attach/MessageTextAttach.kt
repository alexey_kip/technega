package ru.kiporskiy.im.core.message.attach

/**
 * Вложение в текст сообщения
 */
interface MessageTextAttach {

    enum class Type {
        /**
         * Жирный текст
         */
        BOLD,

        /**
         * Курсив
         */
        ITALIC,

        /**
         * URL адрес
         */
        URL
    }

    /**
     * Символы, входящие во сложение
     */
    fun getPosition(): IntRange

    /**
     * Тип вложения
     */
    fun getType(): Type

}
