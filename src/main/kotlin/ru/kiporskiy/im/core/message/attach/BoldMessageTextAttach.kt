package ru.kiporskiy.im.core.message.attach

/**
 * Жирный текст
 */
data class BoldMessageTextAttach(private val range: IntRange) : MessageTextAttach {

    override fun getPosition() = range

    override fun getType() = MessageTextAttach.Type.BOLD

}
