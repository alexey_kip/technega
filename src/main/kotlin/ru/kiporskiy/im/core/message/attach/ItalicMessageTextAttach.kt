package ru.kiporskiy.im.core.message.attach

/**
 * Курсив текст
 */
data class ItalicMessageTextAttach(private val range: IntRange) : MessageTextAttach {

    override fun getPosition() = range

    override fun getType() = MessageTextAttach.Type.BOLD

}
