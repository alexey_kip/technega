package ru.kiporskiy.im.core.message

import java.util.*

/**
 * Диалог
 */
class Dialog(
        /**
         * Идентификатор последнего сообщения в диалоге
         */
        val topMessageId: Message.ID,
        /**
         * Идентификатор последнего прочитанного входящего сообщения
         */
        val lastReadMessageIn: Int,
        /**
         * Идентификатор последнего прочитанного исходящего сообщения
         */
        val lastReadMessageOut: Int,
        /**
         * Количество непрочитанных сообщений
         */
        val unreadCount: Int) {

    /**
     * Отсортированные сообщения диалога
     */
    private val messages: Map<Message.ID, Message> = TreeMap()

}
