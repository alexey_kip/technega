package ru.kiporskiy.im.core.message

import ru.kiporskiy.im.core.peer.PeerId

class ConversationStorage {

    /**
     * Беседы
     */
    private val conversations = ArrayList<Conversation>()

    /**
     * Диалоги
     */
    private val dialogs = HashMap<PeerId, Dialog>()

}
