package ru.kiporskiy.im.core.message

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.core.message.attach.MessageTextAttach

/**
 * Содержимое текстового сообщения
 */
data class MessageText(private val _text: String,
                       private val _attaches: Array<MessageTextAttach>) {

    val text: String = _text

    val attaches: Array<MessageTextAttach>

    companion object {
        private val log = LoggerFactory.getLogger(MessageText::class.java)
    }

    init {
        val textLength = this.text.length
        val allAttachesCorrect = _attaches.all { attach -> attach.getPosition().last < textLength }
        if (allAttachesCorrect) {
            this.attaches = _attaches
        } else {
            log.warn("Contains incorrect attaches. Text `$_text`. Attaches: $_attaches")
            this.attaches = _attaches
                    .filter { attach -> attach.getPosition().last < textLength }
                    .toTypedArray()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MessageText

        if (text != other.text) return false
        if (!attaches.contentEquals(other.attaches)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + attaches.contentHashCode()
        return result
    }
}
