package ru.kiporskiy.im.core.message.attach

/**
 * URL адрес
 */
data class UrlMessageTextAttach(private val range: IntRange) : MessageTextAttach {

    override fun getPosition() = range

    override fun getType() = MessageTextAttach.Type.BOLD

}
