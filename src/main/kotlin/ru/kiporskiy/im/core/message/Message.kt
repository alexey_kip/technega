package ru.kiporskiy.im.core.message

import ru.kiporskiy.im.core.peer.PeerId

/**
 * Сообщение мессенджера
 */
interface Message {

    /**
     * Идентификатор сообщения
     */
    data class ID(val number: Int, val peerId: PeerId) : Comparable<ID> {

        override fun compareTo(other: ID): Int {
            if (this.peerId != other.peerId) {
                throw IllegalArgumentException()
            }
            return number.compareTo(other.number)
        }

    }

    /**
     * Получить идентификатор сообщения
     */
    fun getId(): ID
}
