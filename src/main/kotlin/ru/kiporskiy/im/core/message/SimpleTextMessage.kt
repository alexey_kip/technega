package ru.kiporskiy.im.core.message

import ru.kiporskiy.im.core.peer.PeerId

data class SimpleTextMessage(
        private val id: Message.ID,
        private val text: MessageText,
        private val replyTo: Message.ID? = null,
        private val forwardedFrom: PeerId?) : Message {

    override fun getId() = id

}
