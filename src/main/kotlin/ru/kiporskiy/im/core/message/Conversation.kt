package ru.kiporskiy.im.core.message

import ru.kiporskiy.im.core.peer.PeerId

/**
 * Открытая беседа
 */
data class Conversation(
        /**
         * Идентификатор пира, диалог с которым открыт для беседы
         */
        val peerId: PeerId,
        /**
         * Беседа помечена, как важная
         */
        val isImportant: Boolean,
        /**
         * Порядок при сортировке диалогов
         */
        val order: Byte = -1)
