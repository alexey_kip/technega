package ru.kiporskiy.im.core.chat

import ru.kiporskiy.im.core.peer.Peer
import ru.kiporskiy.im.core.user.UserId
import java.net.URL

interface Chat : Peer {

    /**
     * Создатель чата
     */
    val creator: UserId

    /**
     * Участники
     */
    val participants: Map<UserId, ChatParticipant>

    /**
     * URL чата
     */
    val chatLink: URL?

    /**
     * Название чата
     */
    val name: String

}
