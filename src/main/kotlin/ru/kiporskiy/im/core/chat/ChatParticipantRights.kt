package ru.kiporskiy.im.core.chat

/**
 * Доп.права участника чата
 */
data class ChatParticipantRights(val isCreator: Boolean,
                                 val isAdmin: Boolean)
