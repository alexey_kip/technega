package ru.kiporskiy.im.core.chat

import ru.kiporskiy.im.core.peer.PeerType
import ru.kiporskiy.im.core.peer.settings.NotifySettings
import ru.kiporskiy.im.core.user.UserId
import java.net.URL

data class DefaultChat(override val id: ChatId,
                       override val name: String,
                       override val creator: UserId,
                       override val notifySettings: NotifySettings = NotifySettings.DEFAULT,
                       override val participants: Map<UserId, ChatParticipant>,
                       override val chatLink: URL? = null) : Chat {

    override val type = PeerType.CHAT
}
