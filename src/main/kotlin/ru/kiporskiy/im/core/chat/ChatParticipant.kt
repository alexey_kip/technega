package ru.kiporskiy.im.core.chat

import ru.kiporskiy.im.core.user.UserId

/**
 * Участник чата
 */
data class ChatParticipant(val chatId: ChatId,
                           val userId: UserId,
                           val invitedBy: UserId,
                           val kickedBy: UserId? = null,
                           val left: Boolean = false,
                           val limits: ChatParticipantLimits = ChatParticipantLimits(true),
                           val rights: ChatParticipantRights = ChatParticipantRights(false, false))
