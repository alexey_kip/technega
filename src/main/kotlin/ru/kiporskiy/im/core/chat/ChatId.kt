package ru.kiporskiy.im.core.chat

import ru.kiporskiy.im.core.peer.PeerId

/**
 * Идентификатор чата
 */
data class ChatId(override val id: Int) : PeerId
