package ru.kiporskiy.im.core.chat

/**
 * Ограничения для участника чата
 */
data class ChatParticipantLimits(val sendMessage: Boolean)
