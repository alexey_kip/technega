package ru.kiporskiy.im

import ru.kiporskiy.im.domain.AuthMediator
import ru.kiporskiy.im.domain.HelpMediator
import ru.kiporskiy.im.domain.Mediator
import ru.kiporskiy.im.domain.service.AuthService
import ru.kiporskiy.im.domain.service.ConfigService
import ru.kiporskiy.im.domain.service.MasterService
import ru.kiporskiy.im.domain.service.PingPongService
import ru.kiporskiy.im.messaging.MessageDispatcher
import ru.kiporskiy.im.messaging.MessageDispatcherImpl
import ru.kiporskiy.im.messaging.MessageManager
import ru.kiporskiy.im.messaging.dto.TLObjectUtilsImpl
import ru.kiporskiy.im.messaging.mediator.MessagingAuthMediator
import ru.kiporskiy.im.messaging.mediator.MessagingHelpMediator
import ru.kiporskiy.im.messaging.router.AcknowledgeMessageRouter
import ru.kiporskiy.im.messaging.router.ResponseMessageRouter
import ru.kiporskiy.im.messaging.router.ServiceMessageRouter
import ru.kiporskiy.im.transport.*

/**
 * Инструмент для инициализации всех нужных компонентов
 *
 * @param initialDc адреса датацентров для подключения к серверу
 * @param transport используемый транспорт для общения с сервером
 */
class TechnoClientBootstrap(
    private val initialDc: Array<DcAddress>,
    private val transport: ConnectionTransport = RSocketTcpConnectionTransport()
) {

    fun run() {
        ServersManager.setServerAddresses(initialDc.toHashSet())

        //сериализация/десериализация TL объектов
        val codec = TLObjectUtilsImpl()
        //менеджер сообщений, запланированных к отправке на сервер
        val msgManager = MessageManager()
        //"первая линия" для всех входящих байтов - десериализует объект и отдает для дальшнейшей обработки роутеру
        val recipient = RecipientImpl(codec)
        //класс для маршрутизации ответов, получаемых от сервера
        val responseMessageRouter = ResponseMessageRouter()
        val sender = Connection(transport, recipient)
        //класс, с помощью которого происходит "планирование" отправки запроса на сервер
        val dispatcher: MessageDispatcher =
            MessageDispatcherImpl(sender, codec, msgManager, responseMessageRouter, sender)

        //посредник между бизнес логикой и транспортным уровнем для отправки запросов авторизации
        val authMediator: AuthMediator = MessagingAuthMediator(dispatcher)
        //посредник между бизнес логикой и транспортным уровнем для отправки вспомогательных запросов
        val helpMediator: HelpMediator = MessagingHelpMediator(dispatcher)
        //класс для объединения всех "посредников"
        val actor = Mediator(helpMediator, authMediator)

        //сервис авторизации
        val authService = AuthService(actor)
        //сервис пинга
        val pingPongService = PingPongService(actor, sender)
        //сервис конфигурации
        val configService = ConfigService(actor)

        //маршрутизация сервисных ответов от сервера
        val serviceMessageRouter = ServiceMessageRouter(pingPongService)
        //маршрутизация подтверждений о получении от сервера
        val acknowledgeMessageRouter = AcknowledgeMessageRouter(msgManager)
        recipient.addMsgHandler(acknowledgeMessageRouter)
        recipient.addMsgHandler(serviceMessageRouter)
        recipient.addMsgHandler(responseMessageRouter)

        MasterService.initServices(authService, configService, pingPongService)
    }

}
