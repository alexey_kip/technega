package ru.kiporskiy.im.transport

import ru.kiporskiy.im.transport.binary.ByteWrapper

/**
 * Класс для получения данных от сервера и передачи их в бизнес-логику приложения
 */
interface Recipient {

    /**
     * Обработать полученные от сервера данные
     */
    fun inputData(data: ByteWrapper)

}
