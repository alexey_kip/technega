package ru.kiporskiy.im.transport

import io.rsocket.Payload
import io.rsocket.util.ByteBufPayload
import io.rsocket.util.DefaultPayload
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import ru.kiporskiy.im.domain.ServerConnectionStatus
import ru.kiporskiy.im.event.EventManager
import ru.kiporskiy.im.help.Loggers
import ru.kiporskiy.im.transport.binary.ByteBufWrapper
import ru.kiporskiy.im.transport.binary.ByteWrapper
import ru.kiporskiy.im.transport.event.ConnectionEvent
import java.time.Duration
import java.time.Instant
import java.util.concurrent.ThreadLocalRandom

/**
 * Класс, отвечающий за поддержание подключения к серверу
 */
class Connection(transport: ConnectionTransport, recipient: Recipient) : Sender, ServerConnectionStatus {

    companion object {
        private val log = LoggerFactory.getLogger(Connection::class.java)

        /**
         * Время переподключения к серверу
         */
        val RECONNECTING_TIME = Duration.ofMillis(500L)

        /**
         * Задержка между проверками покдлчюения к интернету
         */
        val CHECK_INTERNET_DELAY = Duration.ofSeconds(2L)
    }

    /**
     * Состояние подключения к серверу (подключен, отключен, отсутствует интерент соединение)
     */
    private var state: ConnectionState = ConnectionStateDisconnected(Instant.now())

    /**
     * Транспорт для подключения к серверу
     */
    private val connectionTransport: ConnectionTransport = transport

    /**
     * При инициализации для ConnectionTransport добавляются задания, которые нужно выполнить при подключении к серверу,
     * отключении от него и при получении новых данных
     */
    init {
        this.connectionTransport.onConnected(this::onConnected)
        this.connectionTransport.onDisconnected(this::onDisconnected)
        this.connectionTransport.onInputPayload { recipient.inputData(toBuff(it)) }

        /**
         * Запуск двух заданий:
         * 1. выполить подключение к серверу, если клиент отключился
         */
        GlobalScope.launch(Dispatchers.IO) {
            this@Connection.connect()
        }

        /**
         * 2. проверить доступность интерсента, если не удается установить соединение с сервером.
         */
        GlobalScope.launch(Dispatchers.IO) {
            this@Connection.checkInternet()
        }
    }

    /**
     * Преобразование Payload в буфера байтов
     */
    private fun toBuff(payload: Payload): ByteBufWrapper {
        return when (payload) {
            is ByteBufPayload -> ByteBufWrapper(payload.data())
            is DefaultPayload -> ByteBufWrapper(payload.data())
            else -> throw IllegalArgumentException()
        }
    }

    private fun onConnected(address: DcAddress) {
        log.info(Loggers.TRANSPORT_LOG, "Подключение к серверу выполнено {}", address)

        this.state = ConnectionStateConnected(address)
        EventManager.handleEvent(ConnectionEvent(true))
    }

    private fun onDisconnected() {
        log.info(Loggers.TRANSPORT_LOG, "Отключен от сервера")

        if (this.state !is ConnectionStateDisconnected)
            this.state = ConnectionStateDisconnected(Instant.now())
        EventManager.handleEvent(ConnectionEvent(false))
    }


    /**
     * Отправить данные на сервер
     */
    override fun send(data: ByteWrapper, onFinished: ((success: Boolean) -> Unit)?) {
        if (this.state !is ConnectionStateConnected) {
            onFinished?.invoke(false)
            return
        }

        try {
            this.connectionTransport.write(data.getPayload(), onFinished)
        } catch (writeDataException: Exception) {
            onFinished?.invoke(false)
        }
    }

    /**
     * Попытка подключения к серверу.
     * Запускается бесконечный цикл с небольшой задержкой между итерациями. При каждом выполнении происходит проверка
     * подключения клиентом к серверу, если подключение отсутствует - выполняется попытка подключения.
     */
    private suspend fun connect() {
        var connectionsCounter = 0
        while (true) {
            //дефолтная задержка между подключениями
            delay(RECONNECTING_TIME.toMillis())

            //если клиент уже подключен к серверу или отсутствует интернет - ничего не делать
            if (this@Connection.state is ConnectionStateConnected || this@Connection.state is ConnectionStateNoInternet) {
                connectionsCounter = 0
                continue
            }

            //если количество попыток подключиться кратно 20, то выдержать долгую паузу перед следующим подключением
            if (connectionsCounter > 0 && connectionsCounter % 20 == 0) {
                val waitingTime = ThreadLocalRandom.current().nextLong(1000, 5000)
                log.info(Loggers.TRANSPORT_LOG, "Переподключение к серверу через {} сек", waitingTime / 1000)
                delay(waitingTime)
            }

            //поиск сервера для подключения и попытка подключиться
            try {
                val server = ServersManager.get(connectionsCounter++ > 0)
                log.info(Loggers.TRANSPORT_LOG, "Подключение к серверу {}", server)
                if (this@Connection.connectionTransport.connect(server)) {
                    connectionsCounter = 0
                }
            } catch (allErrors: Exception) {
                log.warn("Ошибка при подключении к серверу: ", allErrors)
            }
        }
    }

    /**
     * Периодическая проверка подключения к интернету
     */
    private suspend fun checkInternet() {
        while (true) {
            //дефолтная задержка между проверками
            delay(CHECK_INTERNET_DELAY.toMillis())

            //если клиент подключен к серверу, то интернет есть
            if (this@Connection.state is ConnectionStateConnected) {
                continue
            }

            //проверка интернета и изменение статуса
            val internet = InternetConnectionTester.check()
            val state = this@Connection.state
            if (internet && state is ConnectionStateNoInternet) {
                log.info("Подключение к интернету: ОК")
                this@Connection.state = ConnectionStateDisconnected(state.disconnectedTime)
            } else if (!internet && state is ConnectionStateDisconnected) {
                log.info("Подключение к интернету: ОТСУТСТВУЕТ")
                this@Connection.state = ConnectionStateNoInternet(state.disconnectedTime)
            }
        }
    }

    override fun isConnected() = this.state is ConnectionStateConnected
}
