package ru.kiporskiy.im.transport

import io.rsocket.Payload
import io.rsocket.RSocket
import io.rsocket.core.RSocketConnector
import io.rsocket.transport.netty.client.TcpClientTransport
import org.slf4j.LoggerFactory
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Flux
import reactor.netty.tcp.TcpClient

/**
 * Tcp транспорт, основанный на RSocket
 */
class RSocketTcpConnectionTransport : ConnectionTransport {

    companion object {
        private val log = LoggerFactory.getLogger(RSocketTcpConnectionTransport::class.java)
    }

    /**
     * Задание, выполняемое при подключении к серверу
     */
    private var onConnectedTask: ((DcAddress) -> Unit)? = null

    /**
     * Задание, выполняемое при отключении от сервера
     */
    private var onDisconnectedTask: (() -> Unit)? = null

    /**
     * Задание, выполняемое при получении данных от сервера
     */
    private var onInputPayloadTask: ((Payload) -> Unit)? = null


    /**
     * Активное подключение
     */
    private var socket: RSocket? = null

    /**
     * Поток для чтения ответов сервера
     */
    private var recipientStream: Flux<Payload>? = null

    /**
     * Адрес сервера, к которому подключен клиент
     */
    private var address: DcAddress? = null

    /**
     * Поток для отправки данных на сервер
     */
    private var senderStream: EmitterProcessor<Payload>? = null


    @Synchronized
    override fun connect(address: DcAddress): Boolean {
        this.socket?.dispose()
        this.address = null

        //создание транспорта
        val transport = TcpClient.create()
                .host(address.address)
                .port(address.port)
                .doOnConnected {
                    onConnectedTask?.invoke(address)
                }
                .doOnDisconnected {
                    onDisconnectedTask?.invoke()
                }

        //инициализация подключения
        val rSocketMono = RSocketConnector
            .connectWith(TcpClientTransport.create(transport))

        return try {

            //попытаться подключиться к новому серверу
            this.socket = rSocketMono.block()

            //запоминаем адрес сервера
            this.address = address

            //поток для записи данных
            this.senderStream = EmitterProcessor.create()

            //поток для чтения данных
            this.recipientStream = this.socket?.requestChannel(this.senderStream!!)

            this.recipientStream?.doOnNext { onInputPayloadTask?.invoke(it) }?.subscribe()

            this.socket != null
        } catch (connectionRefused: RuntimeException) {
            false
        }
    }

    override fun write(data: Payload, onFinished: ((success: Boolean) -> Unit)?) {
        val currentSocket = this.socket
        val currentStream = this.senderStream

        if (currentSocket == null || currentStream == null) {
            onFinished?.invoke(false)
        } else {
            try {
                currentStream.onNext(data)
                onFinished?.invoke(true)
            } catch (error: Exception) {
                log.warn("Ошибка при отправке данных на сервер", error)
                onFinished?.invoke(false)
            }
        }
    }

    override fun onConnected(task: (DcAddress) -> Unit) {
        this.onConnectedTask = task
    }

    override fun onDisconnected(task: () -> Unit) {
        this.onDisconnectedTask = task
    }

    override fun onInputPayload(task: (Payload) -> Unit) {
        this.onInputPayloadTask = task
    }
}
