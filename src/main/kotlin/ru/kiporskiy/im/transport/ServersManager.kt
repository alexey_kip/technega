package ru.kiporskiy.im.transport

import org.slf4j.LoggerFactory
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Менеджер адресов серверов.
 * Хранит список адресов серверов для подключения и содержит метод для получения этих адресов.
 */
object ServersManager {
    private val log = LoggerFactory.getLogger(ServersManager::class.java)

    /**
     * Максимальное количество попыток переподключения подряд к одному серверу
     */
    const val MAX_CONNECTIONS_COUNTER = 20

    /**
     * Доступные для подключения адреса серверов
     */
    private val addressesList = HashSet<DcAddress>()

    /**
     * Блокировка для синхронизации всех операций
     */
    private val lock = ReentrantLock()


    /**
     * Последний отданный датацентр
     */
    private var lastAddress: DcAddress? = null

    /**
     * Счетчик количества запросов последнего датацентра
     */
    private var counter = 0

    fun reset() {
        addressesList.clear()
        lastAddress = null
        counter = 0
    }


    /**
     * Получить адрес датацентра.
     * Выбирается случайный сервер. Теперь он активный и будет отдаваться каждый раз. Если 20 раз подряд подключиться
     * к серверу не удается, то выбирается следующий случайный адрес.
     *
     * @param onDisconnected true, если запрос адреса в связи с переподключением к серверу
     */
    fun get(onDisconnected: Boolean = true): DcAddress {
        lock.withLock {
            if (onDisconnected) counter++

            //исключение, когда нет ни одного адреса
            if (addressesList.isEmpty()) {
                throw IllegalStateException("Не задано ни одного адреса датацентра")
            }

            var nextDc = lastAddress

            //если было произведено слишком много попыток подключения к серверу, либо не было вообще попыток - получить
            //другой адрес датацентра
            if (counter > MAX_CONNECTIONS_COUNTER || nextDc == null) {
                nextDc = getNextDc()
                lastAddress = nextDc
                counter = 1
            }

            if (log.isDebugEnabled) {
                log.debug("Следующий адрес для подключения: {}. Всего попыток подключиться к серверу {}",
                        nextDc, counter)
            }

            return nextDc
        }
    }

    private fun getNextDc(): DcAddress {
        return if (addressesList.size == 1) {
            addressesList.iterator().next()
        } else {
            addressesList.shuffled().first { address -> address != lastAddress }
        }
    }

    /**
     * Задать новый список адресов серверов
     */
    fun setServerAddresses(dc: Set<DcAddress>) {
        if (dc.isEmpty()) {
            throw IllegalStateException("Список адресов датацентров не может быть пустым")
        }

        if (dc == addressesList) {
            return
        }

        lock.lock()
        try {
            addressesList.clear()
            addressesList.addAll(dc)
            counter = 0
            lastAddress = getNextDc()
            if (log.isDebugEnabled) {
                log.debug("Новый список адресов серверов: {}", addressesList)
            }
        } finally {
            lock.unlock()
        }
    }

}
