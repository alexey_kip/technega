package ru.kiporskiy.im.transport

import java.time.Instant

/**
 * Состояние подключения к серверам
 */
sealed class ConnectionState

/**
 * Отключено
 */
data class ConnectionStateDisconnected(
        /**
         * Время отключения от сервера
         */
        val disconnectedTime: Instant) : ConnectionState()

/**
 * Подключено
 */
data class ConnectionStateConnected(
        /**
         * Адрес датацентра, к которому подключен сервер
         */
        val server: DcAddress) : ConnectionState()

/**
 * Отсутствует подключение к интернету
 */
data class ConnectionStateNoInternet(
        /**
         * Время отключения от интернета
         */
        val disconnectedTime: Instant) : ConnectionState()
