/**
 * Классы пакета отвечают за поддержание подключения к серверу, отправки данных на сервер, получения данных с сервера
 * и хранение адресов серверов.
 * Основные интерфейсы:
 * {@link ru.kiporskiy.im.transport.Sender} - интерфейс содержит методы для отправки байтов на сервер.
 * {@link ru.kiporskiy.im.transport.Recipient} - интерфейс содержит методы обработки поступающих от серчера байтов.
 * {@link ru.kiporskiy.im.transport.ServersManager} - хранитель адресов для доступа к серверу.
 * {@link ru.kiporskiy.im.transport.InternetConnectionTester} - инструмент для проверки наличия интернет соединения.
 * {@link ru.kiporskiy.im.transport.Connection} - класс для поддержания подключения к серверу.
 * {@link ru.kiporskiy.im.transport.ConnectionTransport} - инструмент для взаимодействия с сервером на транспортном
 * уровне.
 */
package ru.kiporskiy.im.transport;
