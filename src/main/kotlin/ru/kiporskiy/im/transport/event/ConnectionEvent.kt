package ru.kiporskiy.im.transport.event

import ru.kiporskiy.im.event.Event
import ru.kiporskiy.im.event.EventType

/**
 * Событие, связанное с подключением клиента к сереру и отключением от него
 * true - выполнено подключение к серверу, false - произошло отключение от сервера
 */
data class ConnectionEvent(val connected: Boolean) : Event {

    companion object {
        val EVENT_TYPE = EventType.getDefault("CONNECTION_EVENT")
    }

    override fun getType() = EVENT_TYPE

}
