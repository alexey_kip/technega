package ru.kiporskiy.im.transport

import ru.kiporskiy.im.transport.binary.ByteWrapper

/**
 * Отправитель сообщений на сервер.
 * Записывает сообщения в канал и отправляет по сети
 */
interface Sender {

    /**
     * Отправить данные на сервер
     *
     * @param data данные для отправки
     * @param onFinished задание, которое нужно выполнить после отправки данных
     */
    fun send(data: ByteWrapper, onFinished: ((success: Boolean) -> Unit)? = null)

}
