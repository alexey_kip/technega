package ru.kiporskiy.im.transport

import org.slf4j.LoggerFactory
import ru.kiporskiy.im.messaging.Message
import ru.kiporskiy.im.messaging.MessageRouterManager
import ru.kiporskiy.im.messaging.dto.TLObjectUtils
import ru.kiporskiy.im.messaging.router.AcknowledgeMessageRouter
import ru.kiporskiy.im.messaging.router.MessageRouter
import ru.kiporskiy.im.transport.binary.ByteWrapper
import java.util.*

/**
 * Получает данные, десериализует их и полученный список сообщений отдает на обработку менеджеру сообщений.
 */
class RecipientImpl(private val utils: TLObjectUtils) : Recipient, MessageRouterManager {

    companion object {
        private val log = LoggerFactory.getLogger(RecipientImpl::class.java)
    }

    /**
     * Список обработчиков запросов
     */
    private val msgHandlers = LinkedList<MessageRouter>()

    override fun inputData(data: ByteWrapper) {
        val messages = utils.deserialize(data)

        messages.forEach { m -> if (!msgHandlers.any { h -> h.routing(m) }) this.unknownMessage(m) }
    }

    private fun unknownMessage(msg: Message) {
        log.warn("Unknown message. Id: ${msg.id}, body: [${msg.data}]")
    }

    override fun addMsgHandler(router: MessageRouter) {
        if (msgHandlers.isEmpty())
            require(router is AcknowledgeMessageRouter) { "Первым всегда должен идти AcknowledgeMessageRouter!!!" }

        msgHandlers += router
    }
}
