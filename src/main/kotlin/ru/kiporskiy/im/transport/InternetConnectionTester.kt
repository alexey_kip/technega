package ru.kiporskiy.im.transport

import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

/**
 * Инструмент для проверки наличия интернет соединения
 */
object InternetConnectionTester {

    /**
     * Хост для подключения
     */
    private const val HOST = "8.8.8.8"

    /**
     * Хост для подключения
     */
    private const val PORT = 53

    /**
     * Максимальное время ожидания подключения
     */
    private const val TIMEOUT = 1500

    @Synchronized
    fun check(): Boolean {
        return try {
            val sock = Socket()
            sock.connect(InetSocketAddress(HOST, PORT), TIMEOUT)
            sock.close()
            true
        } catch (e: IOException) {
            false
        }
    }

}
