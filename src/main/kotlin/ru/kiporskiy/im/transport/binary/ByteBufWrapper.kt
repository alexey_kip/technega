package ru.kiporskiy.im.transport.binary

import io.netty.buffer.ByteBuf
import io.netty.buffer.PooledByteBufAllocator
import io.rsocket.util.ByteBufPayload
import java.nio.charset.Charset

/**
 * Обертка над ByteBuf (netty) буфером. После использования не забыть почистить буфер.
 * Запись-чтение данных проходит в режиме LittleEndian.
 */
internal class ByteBufWrapper private constructor() : ByteWrapper {

    companion object {
        /**
         * Кодировка строк
         */
        private val DEFAULT_CHARSET = Charset.forName("UTF-8")
    }

    /**
     * Буфер, с которым происходит работа на самом деле
     */
    private lateinit var buffer: ByteBuf


    /**
     * Создать новую обертку с уже готовым буфером
     */
    constructor(buffer: ByteBuf) : this() {
        this.buffer = buffer
    }

    /**
     * Создать новый буфер начальной вместимостью initCapacity. Если direct - true, то буфер будет создан
     * вне хипа.
     */
    constructor(initCapacity: Int, direct: Boolean = false) : this() {
        try {
            if (direct) {
                this.buffer = PooledByteBufAllocator.DEFAULT.directBuffer(initCapacity)
            } else {
                this.buffer = PooledByteBufAllocator.DEFAULT.heapBuffer(initCapacity)
            }
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    /**
     * Создать новый буфер начальной вместимостью initCapacity и максимальной вместимостью maxCapacity.
     * Если direct - true, то буфер будет создан вне хипа.
     */
    constructor(initCapacity: Int, maxCapacity: Int, direct: Boolean = false) : this() {
        try {
            if (direct) {
                this.buffer = PooledByteBufAllocator.DEFAULT.directBuffer(initCapacity, maxCapacity)
            } else {
                this.buffer = PooledByteBufAllocator.DEFAULT.heapBuffer(initCapacity, maxCapacity)
            }
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }


    override fun write(value: Int): ByteWrapper {
        try {
            this.buffer.writeIntLE(value)
            return this
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(value: Long): ByteWrapper {
        try {
            this.buffer.writeLongLE(value)
            return this
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(value: Boolean): ByteWrapper {
        try {
            this.buffer.writeBoolean(value)
            return this
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(value: ByteArray, light: Boolean): ByteWrapper {
        try {
            if (light) {
                this.buffer.writeBytes(value)
            } else {
                //длина данных - количество чанков по 4 байта
                val length = value.size
                //изначально, размер занимает только 1 байт
                var lengthBytes = 1
                if (length >= 254) {
                    //если чанков больше или равно, чем 254 (не войдет в 1 байт), то длина будет описана 4 байтами
                    lengthBytes = 4
                    //где первый байт равен 254, как признак того, что размер шифруется 4 байтами
                    this.buffer.writeByte(254)
                    //дальше 3 байта количества чанков
                    this.buffer.writeMediumLE(length)
                } else {
                    //если количество чанком меньше 254, то записать количество одним байтом
                    this.buffer.writeByte(length)
                }

                //после размера записываются сами данные
                this.buffer.writeBytes(value)

                //если размер пакета не кратный 4, то дозаписать данные в пакет
                if ((length + lengthBytes) % 4 > 0) {
                    this.buffer.writeBytes(ByteArray(4 - (length + lengthBytes) % 4))
                }
            }

            return this
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    /**
     * Закрытие и удаление буфера
     */
    override fun close() {
        try {
            this.buffer.clear().release()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(value: Double): ByteWrapper {
        try {
            this.buffer.writeDoubleLE(value)
            return this
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(value: String): ByteWrapper {
        try {
            val bytes = value.toByteArray(DEFAULT_CHARSET)
            return this.write(bytes, false)
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun write(byteData: ByteWrapper) {
        try {
            if (byteData is ByteBufWrapper) {
                this.buffer.writeBytes(byteData.buffer)
            } else {
                throw IllegalArgumentException()
            }
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }


    override fun readInt(): Int {
        try {
            return this.buffer.readIntLE()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun readLong(): Long {
        try {
            return this.buffer.readLongLE()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun readDouble(): Double {
        try {
            return this.buffer.readDoubleLE()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun readBoolean(): Boolean {
        try {
            return this.buffer.readBoolean()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun readString(): String {
        try {
            val bytes = readBytes()
            return String(bytes, DEFAULT_CHARSET)
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    override fun readBytes(binary: Boolean, bytesSize: Int): ByteArray {
        try {
            val result: ByteArray
            if (binary || bytesSize >= 0) {
                if (bytesSize > -1) {
                    result = ByteArray(bytesSize)
                } else {
                    result = ByteArray(this.buffer.readableBytes())
                }
                this.buffer.readBytes(result)
            } else {
                //1 байт либо количество чанков по 4 байта, на которые поделены входящие данные, либо 254, как признак того,
                //что размер кодируется 4-я байтами
                var length: Int = this.buffer.readByte().toInt()
                var lengthBytes = 1
                if (length < 0) {
                    length += 256
                }

                if (length >= 254) {
                    length = this.buffer.readMediumLE()
                    lengthBytes = 4
                }

                result = ByteArray(length)
                this.buffer.readBytes(result)

                if ((length + lengthBytes) % 4 > 0) {
                    this.buffer.skipBytes(4 - (length + lengthBytes) % 4)
                }
            }

            return result
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }


    /**
     * Текущее количество байтов, записанных в буфер
     */
    override fun size(): Int {
        try {
            return this.buffer.readableBytes()
        } catch (error: Exception) {
            throw BufferStateException(error)
        }
    }

    /**
     * Обернуть буфер в Payload
     */
    override fun getPayload() = ByteBufPayload.create(this.buffer)
}
