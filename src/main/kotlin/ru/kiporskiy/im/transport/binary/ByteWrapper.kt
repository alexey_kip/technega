package ru.kiporskiy.im.transport.binary

import io.rsocket.Payload
import java.io.Closeable

/**
 * Обертка над буфером байтов.
 * Нужна для возможности смены типа буфера байтов. Все данные записываются и читаются в LittleEndian формате.
 * По большому счету, самое сложное - работа с массивами байтов (строки - тоже массивы байтов).
 * Про сериализацию массивов можно почитать тут: https://core.telegram.org/mtproto/serialize#base-types
 */
interface ByteWrapper : Closeable {

    companion object {
        /**
         * Создание буфера по умолчанию
         */
        fun getDefault(): ByteWrapper = ByteBufWrapper(128, false)
    }

    /**
     * Получение payload RSocket из буфера.
     */
    fun getPayload(): Payload

    //Операции на запись байтов------------------------------------------

    fun write(value: Int): ByteWrapper

    fun write(value: Long): ByteWrapper

    fun write(value: Boolean): ByteWrapper

    fun write(value: Double): ByteWrapper

    fun write(value: String): ByteWrapper

    /**
     * Записать массив байтов. если light == false, тогда сначала запишется
     * размер байтов, а потом сами данные, если light == true, то байты будут записаны "как есть".
     */
    fun write(value: ByteArray, light: Boolean = false): ByteWrapper

    fun write(byteData: ByteWrapper)

    //-----------------------------------------------------------------------

    //Оперцияя чтения--------------------------------------------------------

    fun readInt(): Int

    fun readLong(): Long

    fun readDouble(): Double

    fun readString(): String

    fun readBoolean(): Boolean

    /**
     * Прочитать массив байтов из буфера
     *
     * @param binary если true, то сразу считывается массив байтов, если false, то сначала считается длина
     * возможных данных, а потом только сами данные.
     * @param bytesSize сколько данных нужно считать из буфера.
     * Применяется только тогда, когда binary==true.
     */
    fun readBytes(binary: Boolean = false, bytesSize: Int = -1): ByteArray

    //-----------------------------------------------------------------------

    /**
     * Получить размер буфера.
     */
    fun size(): Int
}
