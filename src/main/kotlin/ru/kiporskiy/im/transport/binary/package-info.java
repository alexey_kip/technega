/**
 * Абстракция для работы с байтами, которые передаются по сети.
 * Вся логика работы с байтами оперирует оберткой {@link ru.kiporskiy.im.transport.binary.ByteWrapper}
 */
package ru.kiporskiy.im.transport.binary;
