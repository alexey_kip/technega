package ru.kiporskiy.im.transport.binary

import java.io.IOException

/**
 * Во время записи/чтения буфера произошла ошибка
 */
class BufferStateException(override val cause: Throwable?) : IOException(cause)