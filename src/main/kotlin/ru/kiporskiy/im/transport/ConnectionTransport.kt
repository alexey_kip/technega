package ru.kiporskiy.im.transport

import io.rsocket.Payload

/**
 * Интерфейс транспорта для обмена данными с сервером
 */
interface ConnectionTransport {

    /**
     * Запустить подключение к указанному серверу
     */
    fun connect(address: DcAddress): Boolean

    /**
     * Записать данные
     */
    fun write(data: Payload, onFinished: ((success: Boolean) -> Unit)?)

    /**
     * Задание, выполняемое при подключении к серверу
     */
    fun onConnected(task: (DcAddress) -> Unit)

    /**
     * Задание, выполняемое при отключении от сервера
     */
    fun onDisconnected(task: () -> Unit)

    /**
     * Задание, выполняемое при получении данных от сервера
     */
    fun onInputPayload(task: (Payload) -> Unit)
}
